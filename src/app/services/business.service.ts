import { Injectable } from '@angular/core';
import { FirebaseService } from './firebase.service';
import { Business } from '@models/business';
import { Subject, BehaviorSubject} from 'rxjs';
import { StorageService } from '@services/storage.service';
import { AuthenticationService } from '@auth/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class BusinessService {
  // private businessSubject:Subject<Business> = new Subject<Business | undefined>();
  
  businessSubject$: BehaviorSubject<Business> = new BehaviorSubject<Business>(undefined);

  constructor(private fbServ: FirebaseService, private authService: AuthenticationService
    , private storageService: StorageService)
  {
     this.refreshBusiness();
  }

  /** Reloads the Business info from the database */
  async refreshBusiness()
  { 
    const loggedInUser = this.authService.getUser;
    console.log({loggedInUser});
    if (!loggedInUser) return;

    await this.fbServ.getById('businesses', loggedInUser.uid)
      .then((biz)=>{console.log('businessservice',biz)
        if (biz === undefined) return null;
        this.businessSubject$.next(biz);
      });
  }

  /** Returns the business info */
  getBusiness()
  { if (!this.businessSubject$.value)
    { console.log('this.refreshBusiness()',this.businessSubject$.value)
      this.refreshBusiness();
    }
    return this.businessSubject$.value;
  }

  async getBusiness$()
  { if (!this.businessSubject$.value) await this.refreshBusiness();
    return this.businessSubject$.value;
  }

  updateBusinessObject(biz: Business)
  {
    this.businessSubject$.next(biz);
    console.log('biz saved:', biz);
  }

  async add(obj:Business)
  {
    return await this.fbServ.add('businesses',obj, obj.id);
  }

  async update(id:string,obj)
  {
    return await this.fbServ.update('businesses',id,obj);
  }

  get(filter:Array<string>)
  { 
    return this.fbServ.get('businesses', filter);
  }

  async getFromLocal(): Promise<Business>
  {
    return await this.storageService.get('business');
  }

  async destroySubject()
  {
    await this.storageService.remove('business').then((biz) => {
      console.log('business removed from storage');
      this.businessSubject$.next(null);
      //if (this.businessSubject$) this.businessSubject$.unsubscribe();
    });
  }

  getById(id)
  {
    return this.fbServ.getById('businesses', id);
  }

  addCreditLog(id, log)
  {
    return this.fbServ.updateArray('businesses',id,'creditLoadHistory',log)
  }
}
