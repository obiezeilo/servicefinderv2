import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PayWithPaystackPage } from './pay-with-paystack.page';

describe('PayWithPaystackPage', () => {
  let component: PayWithPaystackPage;
  let fixture: ComponentFixture<PayWithPaystackPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayWithPaystackPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PayWithPaystackPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
