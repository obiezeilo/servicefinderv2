import { Component, OnInit } from '@angular/core';
import { Message, Chat } from '@models/message';
import { MessageService } from '@services/message.service';
import {ToolboxService} from '@services/toolbox.service';
import { Subscription } from 'rxjs';
import { AuthenticationService } from '@auth/authentication.service';

@Component({
  selector: 'app-messages-list',
  templateUrl: './messages-list.page.html',
  styleUrls: ['./messages-list.page.scss'],
})
export class MessagesListPage implements OnInit {
  messages:Message[];
  userId:string;
  messagesSubscription: Subscription;

  constructor(private msgSrv:MessageService, private auth:AuthenticationService
    , private tools: ToolboxService) { }

  ngOnInit() {
      this.userId = this.auth.getUser.uid;
      if (!this.userId) return false;
      console.log(this.userId);

      this.messagesSubscription = this.msgSrv.get$([{property:'userId', operator:'==', value:this.userId}])
        .subscribe( (m) => {
          console.log(m);
          this.messages = [];
          const temp = m as Message[];

          temp.forEach((msg)=>{
            for(let i=0;i<msg.chats.length; i++)
            { if (!msg.chats[i].read)
              {
                if ((msg.chats[i].sender==='user' && msg.userId ===  this.userId)
                  || (msg.chats[i].sender==='business' && msg.userId !==  this.userId) )
                { msg.read=false;console.log('unread');i=msg.chats.length;break}
              }
              else
              {msg.read=true;}
            }

            const each = msg;
              each['timeago'] = this.tools.timeAgo(msg.lastChatAddedTime);
              this.messages.push(each);
          });
        });
  }

  gotoMessage(m)
  {
    console.log(m);
     // mark the latest chat as read
      if (m.read == false)
      { let thechats = m.chats;
        for (var x=0; x<thechats.length; x++)
        {
          if (m.chats[x].read == false)  thechats[x].read=true;
        }
        // m.chats[m.chats.length].read = true;
        console.log(thechats);
        this.msgSrv.update(m.requestId,{chats:thechats})
        .then ((u)=>{console.log('chat marked as read: ', u);
          this.tools.goto('messages',{requestId:m.requestId});})
        .catch((e)=>{console.warn(e)});
      }
      else
        this.tools.goto('messages',{requestId:m.requestId});
  }

  ngOnDestroy()
  {
    this.messagesSubscription.unsubscribe()
  }
}