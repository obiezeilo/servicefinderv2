import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusinessRequestDetailsPage } from './business-request-details.page';

const routes: Routes = [
  {
    path: '',
    component: BusinessRequestDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusinessRequestDetailsPageRoutingModule {}
