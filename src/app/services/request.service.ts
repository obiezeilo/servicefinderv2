import { Injectable } from '@angular/core';
import { FirebaseService, FirestoreQuery} from '@services/firebase.service';
import Request from '@models/request';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  public requestSubject$ = new BehaviorSubject<Request | null>(null);
  constructor(private fbServ: FirebaseService) { }

  async add(obj: Request, id = null) {
    return await this.fbServ.add('requests', obj, id);
  }

  async update(id: string, obj) {
    return await this.fbServ.update('requests', id, obj);
  }

  async delete(id: string) {
    return await this.fbServ.delete('requests', id);
  }

  get(filter: Array<string>, arr?: Array<string>): any {
    return this.fbServ.get('requests', filter, arr, 'createdDate');
  }

  get$(filter: FirestoreQuery[], arr?: Array<string>, orderby?: Array<string>): Observable<Request[]> {
    const orderArray = (orderby) ? [...orderby, 'createdDate'] : ['createdDate'];
    return this.fbServ.get$('requests', filter, arr, orderArray);
  }

  getSub$(filter: FirestoreQuery[], arr?: Array<string>, orderby?: Array<string>): BehaviorSubject<Request[]> {
    const orderArray = (orderby) ? [...orderby, 'createdDate'] : ['createdDate'];
    const sub$ = new BehaviorSubject<Request[] | null>(null);
    this.fbServ.get$('requests', filter, arr, orderArray).subscribe(sub$);
    return sub$;
  }

  async getById(id: string): Promise<Request> {
    return await this.fbServ.getById('requests', id) as Request;
  }

  getById$(id: string): Observable<Request> {
    this.fbServ.getById('requests', id)
      .then((req:Request)=>{
        if (req === undefined) return null;
        this.requestSubject$.next(req);
        console.log('retrieved req: ',req);
      });
    return this.fbServ.getById$('requests', id);
  }
}
