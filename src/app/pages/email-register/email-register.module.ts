import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EmailRegisterPageRoutingModule } from './email-register-routing.module';

import { EmailRegisterPage } from './email-register.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,ReactiveFormsModule,
    EmailRegisterPageRoutingModule
  ],
  declarations: [EmailRegisterPage]
})
export class EmailRegisterPageModule {}
