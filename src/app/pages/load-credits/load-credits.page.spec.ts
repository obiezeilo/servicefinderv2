import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LoadCreditsPage } from './load-credits.page';

describe('LoadCreditsPage', () => {
  let component: LoadCreditsPage;
  let fixture: ComponentFixture<LoadCreditsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadCreditsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LoadCreditsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
