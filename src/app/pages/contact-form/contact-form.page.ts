import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import {ToolboxService } from '@services/toolbox.service';
import { environment } from 'src/environments/environment';
import { first } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { EmailService } from '@services/email.service';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.page.html',
  styleUrls: ['./contact-form.page.scss'],
})
export class ContactFormPage implements OnInit {

   public contactForm: FormGroup;
    user:any;
   constructor( private formBuilder: FormBuilder , private http: HttpClient, private sendmail: EmailService,
                private tools: ToolboxService, private route: ActivatedRoute, private router: Router)
  {
    this.route.queryParams.subscribe(() => {
      if (this.router.getCurrentNavigation().extras.state)
      // tslint:disable-next-line: one-line
      {
        console.log(this.router.getCurrentNavigation().extras.state);
        this.user = this.router.getCurrentNavigation().extras.state.params;
        this.contactForm = this.formBuilder.group({
          /* uname: [usr.name ? usr.name : '', Validators.required],
          // subject : ['', Validators.required],
          email : new FormControl(usr.email ? usr.email : '', Validators.compose([
            Validators.required,
            Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
          ])), */
          message: ['', Validators.required],
        });
      }
    });
  }

  ngOnInit() {}

  async logForm() {
    await this.tools.showLoading();
    const emailtext = `Service Finder contact form
    ----------------------------
    Sent on ${new Date(Date.now()).toLocaleString()}
    Name: ${this.user.name}
    Email: ${this.user.name}
    Message: ${this.contactForm.value.message}
    `;

    const emailhtml = `<h3>Service Finder contact form</h3><hr>
    Sent on ${new Date(Date.now()).toLocaleString()}
    <p><b>Name:</b> ${this.user.name}</p>
    <p><b>Email:</b> ${this.user.name}</p>
    <p><b>Message:</b> ${this.contactForm.value.message}</p>
    `;

    this.sendmail.sendEmail(null,null,'info@servicefinderapp.com',null
      ,'Service Finder app form submission',emailtext,emailhtml)
      .then(data => {
        console.log({data});
        this.tools.showToast('Message sent');
        this.tools.hideLoading();
        this.tools.goto('/tabs/settings');
      }).catch((e) => {
        this.tools.showAlert('Error', e.message);
      });
  }
}