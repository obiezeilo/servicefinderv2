export default class Request {
    id?: string;
    category: string;
    userId: string;
    userName?: string;
    email?: string;
    city: string;
    state: string;
    zip: string;
    latitude:number;
    longitude:number;
    country: string;
    details: string;
    edited=false;
    createdDate: number;
    status:string;
    statusDate:number;
    quoteCount=0;
    rating:number=null;
    ratingNotes:string;
    businessId:string;
    acceptedQuoteAmount:number;
    acceptedQuoteId:string;
    media:string[];
    businessesNotified:number;
}
