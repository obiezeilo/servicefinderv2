import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import {RequestFormPageModule} from '../request-form/request-form.module';
import { CompletedRequestDetailsPageRoutingModule } from './completed-request-details-routing.module';

import { CompletedRequestDetailsPage } from './completed-request-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CompletedRequestDetailsPageRoutingModule,
    RequestFormPageModule
  ],
  declarations: [CompletedRequestDetailsPage]
})
export class CompletedRequestDetailsPageModule {}
