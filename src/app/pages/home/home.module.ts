import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomePageRoutingModule } from './home-routing.module';

import {RequestFormPageModule} from '../request-form/request-form.module';
import { ComponentsModule } from '../../components/components.module';
import { HomePage } from './home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    ComponentsModule,
    RequestFormPageModule
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
