import { Injectable } from '@angular/core';
import { AlertController, ToastController, LoadingController, NavController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { FirebaseService } from '@services/firebase.service';
import { HttpClient } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { AuthenticateService } from '@services/auth.service';
import { StorageService } from '@services/storage.service';
import { Location } from '@angular/common';
import { UserService } from './user.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ToolboxService {
  public loading:any;

  constructor(private alertCtrl: AlertController,private router: Router
    , private navCtrl: NavController, private userSrv: UserService
    , private toastCtrl: ToastController,private http: HttpClient,private location: Location
    ,public loadingCtrl: LoadingController, private storageService: StorageService
    , private fbServ: FirebaseService, private authService:AuthenticateService) {}

    async init() {
      // If using, define drivers here: await this.storage.defineDriver(/*...*/);
      // const storage = await this.storage.create();
      // this._storage = storage;
    }

  async showAlert(header:string=null, msg:string)
  {
    const alert = await this.alertCtrl.create({
      header: header,
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }


  // Navigate to another page
  goto(page: string, _params?: any, setroot?: boolean)
  {
    let navigationExtras: NavigationExtras
    if (_params)
    { navigationExtras = {
        state: {params: _params}
      };
    }
    if (setroot)
      this.navCtrl.navigateRoot([page]);
    else
      this.router.navigate([page], navigationExtras);
  }

  async getCurrency(code=null)
  { console.log('toolboxgetcurren', this.userSrv.userSubject$.value);
    if (this.userSrv.userSubject$.value)
      return (this.userSrv.userSubject$.value.currencyCode && code) ? this.userSrv.userSubject$.value.currencyCode : this.userSrv.userSubject$.value.currencySymbol;
    else return ((await this.storageService.get('countryInfo')).currencysymbol ?? null)
      return null;
  }
// TODO Look for efficient way of storing country info on the device
  async getCountryInfo(c)  // NEEDS TO BE REVISED
  {
    return (await fetch(environment.sfURL + 'countries.php?c=' + c.toUpperCase(), {
      method: 'GET',
      mode: 'cors'
    })).json()
  }

  /**
   * 
   * @returns A JSON list of countriees
   */
  async getCountries()
  {
    return (await fetch(environment.sfURL + 'countries.php', {
      method: 'GET',
      mode: 'cors'
    })).json()
  }

  async getStates(c)
  {
    const obj= await (await fetch(environment.sfURL +  'countrystates.php?c=' + c, {
      method: 'GET',
      mode: 'cors'
    })).json();
    return obj.states;
  }

  async showToast(msg: string): Promise<void> {
    await this.toastCtrl.create({
      message: msg,
      duration: 3000
    }).then(toast => toast.present());
  }

  // Save to local storage
  async saveToLocal(key: string,value:any)
  {
    try {      return await this.storageService.set(key,value)
    } catch (error) {
      console.warn('Error toolbox savetolocal: ', error);
      return error;
    }
  }

  async getFromLocal(key:string)
  {
    return await this.storageService.get(key);
  }

  async refreshLocalStorage(collection:string, storageKey:string, id:string)
  {
    return this.fbServ.getById(collection,id)
      .then((d)=>{
        return this.saveToLocal(storageKey,d);
      })
      .then(()=>{console.log('refreshed ' + storageKey)});
  }

  async showLoading(message = 'Please wait'): Promise<void>
  {
     this.loading= await this.loadingCtrl.create({
      message: message
    });

    return await this.loading.present();
  }

  async hideLoading(): Promise<void>
  {
    if (this.loading) this.loading.dismiss().then(()=>console.log('Loading dismissed'));
  }

  async getLatLng(address:string)
  {
    return await this.http.get(address).pipe(first()).toPromise().then((l)=>{
      if (l && l['status'] === 'OK')
      { console.log('latlng stuff', l);
        return [l['results'][0].geometry.location.lat,
        l['results'][0].geometry.location.lng,
        l['results'][0].address_components[3].long_name,
        l['results'][0].address_components[3].short_name]
      }
      else
      {
        return {status:l['status']};
      }
    }).catch((e)=>{
      console.warn(e);
      return null;
    });
  }

  // https://stackoverflow.com/questions/6108819/javascript-timestamp-to-relative-time
  timeAgo(previous) {

    var msPerMinute = 60 * 1000;
    var msPerHour = msPerMinute * 60;
    var msPerDay = msPerHour * 24;
    var msPerMonth = msPerDay * 30;
    var msPerYear = msPerDay * 365;

    var elapsed = Date.now() - previous;
    var txt = '';
    if (elapsed <= msPerMinute) {
         //return Math.round(elapsed/1000) + ' seconds ago';
         txt= 'less than a minute';
    }

    else if (elapsed < msPerHour) {
      txt= (Math.round(elapsed/msPerMinute) == 1) ? '1 minute' : Math.round(elapsed/msPerMinute) + ' minutes';
    }

    else if (elapsed < msPerDay ) {
      txt = (Math.round(elapsed/msPerHour) == 1) ? '1 hour' : Math.round(elapsed/msPerHour) + ' hours';
    }

    else if (elapsed < msPerMonth) {
      txt = (Math.round(elapsed/msPerDay) == 1) ? '1 day' : Math.round(elapsed/msPerDay) + ' days';
    }

    else if (elapsed < msPerYear) {
      txt = (Math.round(elapsed/msPerMonth) == 1) ? '1 month' : Math.round(elapsed/msPerMonth) + ' months';
    }

    else {
      txt = (Math.round(elapsed/msPerYear) == 1) ? '1 year' : Math.round(elapsed/msPerYear) + ' years';
    }

    return `${txt} ago`;
  }

  logOut(){
    this.authService.logoutUser()
      .then(() => {
        console.log('logout home');
        this.goto('/start');
      })
      .catch(error => {
        console.log({error});
    })
  }

  goBack(){
    this.location.back();
  }


  async getJSONFromURL(url: string)
  {console.log('theURL:',environment.sfURL + url)
    return await (await fetch(environment.sfURL + url, {
      method: 'GET',
      mode: 'cors'
    })).json()
  }
}