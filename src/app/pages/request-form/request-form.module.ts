import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RequestFormPageRoutingModule } from './request-form-routing.module';

import { RequestFormPage } from './request-form.page';
import { TextBoxComponent } from '@components/text-box/text-box.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,ReactiveFormsModule,
    RequestFormPageRoutingModule
  ],
  declarations: [RequestFormPage, TextBoxComponent]
})
export class RequestFormPageModule {}
