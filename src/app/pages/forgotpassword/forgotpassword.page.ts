import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { AuthenticateService } from '@services/auth.service';
import { ToolboxService } from '@services/toolbox.service';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.page.html',
  styleUrls: ['./forgotpassword.page.scss'],
})
export class ForgotpasswordPage implements OnInit {

  validations_form: FormGroup;
  errorMessage: string = '';
  token:string;

  constructor(

    private navCtrl: NavController,
    private authService: AuthenticateService,
    private formBuilder: FormBuilder,
    private tools: ToolboxService

  ) { }

  ngOnInit() {
    this.validations_form = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      
    });
  }

  
  validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Please enter a valid email.' }
    ]
    
  };

  sendEmail(formdata){
    this.tools.showLoading();

    this.authService.forgotpassword(formdata.email).then(res=>{
      this.tools.hideLoading();
      this.tools.showAlert("Instructions sent",`Instructions for resetting your password have been sent to ${formdata.email}.`);
      this.navCtrl.navigateForward('/email-login');
    }).catch((e)=>{
      this.tools.hideLoading();
      console.warn(e);
      this.tools.showAlert("Error",JSON.stringify(e));
    })

  }

}
