import { Injectable } from '@angular/core';
import { Message, Chat } from '@models/message';
import { of } from 'rxjs';
import { FirebaseService, FirestoreQuery} from '../services/firebase.service'

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private fbServ: FirebaseService) { }

  async add(obj:Message)
  {
    return await this.fbServ.add('messages',obj, obj.requestId);
  }

  async addChat(messageId, chat:Chat)
  {
    return await this.fbServ.updateArray('messages',messageId, 'chats',chat).then(()=>{
      this.update(messageId,{lastChatAddedTime:chat.createdDate});
    });
  }

  async update(id:string,obj)
  {
    return await this.fbServ.update('messages',id,obj);
  }

  get(filter:Array<string>)
  { 
    return this.fbServ.get('messages', filter, null,'lastChatAddedTime');
  }

  get$(filter:Array<FirestoreQuery>)
  {
    return this.fbServ.get$('messages', filter, null, ['lastChatAddedTime']);
  }

  getById(id)
  {
    return this.fbServ.getById('messages', id);
  }

  getById$(id)
  {
    return this.fbServ.getById$('messages', id);
  }

  delete(id)
  {
    return this.fbServ.delete('messages',id);
  }
}
