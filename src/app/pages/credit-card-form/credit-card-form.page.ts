import { Component, OnInit } from '@angular/core';
// import { Stripe } from '@ionic-native/stripe/ngx';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ModalController, AlertController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { Business } from '@models/business';
import { BusinessService } from '@services/business.service';
import { ToolboxService } from '@services/toolbox.service';
import { first } from 'rxjs/operators';

declare var Stripe;

@Component({
  selector: 'app-credit-card-form',
  templateUrl: './credit-card-form.page.html',
  styleUrls: ['./credit-card-form.page.scss'],
})
export class CreditCardFormPage implements OnInit {

  stripe = Stripe(environment.stripe.publishableKey);
  card: any;
  isNew = true;
  creditcard: {name: string, last4: number; expMonth: number; expYear: number; brand: string; token: string};
  businessObj: Business;
  name: any;

  constructor(private route: ActivatedRoute, private router: Router, public alertController: AlertController
    ,         private http: HttpClient, private toolbox: ToolboxService
    ,         public modalCtrl: ModalController
    ,         public businessSrv: BusinessService) { }

  ngOnInit() {
    this.creditcard = {name: '', last4: 0, expMonth: 0, expYear: 0, brand: '', token: ''};
    this.creditcard.name = this.name;
    console.log(this.businessObj);
    console.log('creditcard:', this.businessObj.creditCard);
    if (this.businessObj.creditCard == null) {
      setTimeout(() => {this.setupStripe(); }, 10);
    }
  }

  /* async setupStripe_new()// TODO redo the credit card form
  {
    if (!this.businessObj.stripeClientSecret)
    {
      const stripeCustomer = await (await fetch(environment.stripe.newCustomerURL,{
        method:"POST",
        body: JSON.stringify({
          email:this.businessObj.email,
          city:this.businessObj.city,
          state:this.businessObj.state,
          zip:this.businessObj.zip,
          action:"createcustomer",
          description:this.businessObj.name,
          name:this.businessObj.name,
          bizname:this.businessObj.name,
          setupintent:true
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8"
      }
      })).json();
      console.log({stripeCustomer});
      await this.businessSrv.update(this.businessObj.id, {stripeCustomerId:stripeCustomer.id, stripeClientSecret:stripeCustomer.clientsecret});
      await this.businessSrv.refreshBusiness();
    }

    const options = {
      clientSecret: this.businessObj.stripeClientSecret,
      // Fully customizable with appearance API.
      appearance: {},
    };
    
    // Set up Stripe.js and Elements to use in checkout form, passing the client secret obtained in step 2
    const elements = this.stripe.elements(options);
    
    // Create and mount the Payment Element
    const paymentElement = elements.create('payment');
    paymentElement.mount('#payment-element');


    // The submission
    const form = document.getElementById('payment-form');

    form.addEventListener('submit', async (event) => {
      event.preventDefault();
      await this.toolbox.showLoading();

      const {error} = await this.stripe.confirmSetup({
        //`Elements` instance that was used to create the Payment Element
        elements
      });

      if (error) {
        // This point will only be reached if there is an immediate error when
        // confirming the payment. Show error to your customer (for example, payment
        // details incomplete)
        this.toolbox.hideLoading();
        const messageContainer = document.querySelector('#error-message');
        messageContainer.textContent = error.message;
      } else {
        this.toolbox.hideLoading();
        // Your customer will be redirected to your `return_url`. For some payment
        // methods like iDEAL, your customer will be redirected to an intermediate
        // site first to authorize the payment, then redirected to the `return_url`.
      }
    });
  } */

  setupStripe() {

    const elements = this.stripe.elements();
    let style = {
      base: {
        color: '#32325d',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#aab7c4'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    };

    this.card = elements.create('card', { style });

    this.card.mount('#card-element');

    this.card.addEventListener('change', event => {
      let displayError = document.getElementById('card-errors');
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = '';
      }
    });
    
    let form = document.getElementById('payment-form');
    
    form.addEventListener('submit', async event => {
      event.preventDefault();

      await this.toolbox.showLoading();
      this.stripe.createSource(this.card)// create the token
        .then(async result => {

        console.log({result});

        if (result.error) {
          let errorElement = document.getElementById('card-errors');
          errorElement.textContent = result.error.message;
          return false;
        }
        else
        {
          let customerId = (this.businessObj.stripeCustomerId) ? this.businessObj.stripeCustomerId : null
          let cardId, token = result.source.id;
   
          if (customerId == null || customerId == undefined) 
          { customerId = await this.http.post( // new customer
                environment.stripe.newCustomerURL, {
                  email:this.businessObj.email,
                  city:this.businessObj.city,
                  state:this.businessObj.state,
                  zip:this.businessObj.zip,
                  action:"createcustomer",
                  description:this.businessObj.name,
                  name:this.businessObj.name,
                  bizname:this.businessObj.name,
              })
              .toPromise()
              .then(data => {
                console.log({data}); 
                return data['id']; 
              }).catch((e)=>console.warn(e));
          }

          console.log({customerId});
          this.http.post( // new card
              environment.stripe.newCardURL, {
              customerId,
              token,action:'addcreditcard'
              })
              .toPromise().then(custcard => {
                console.log({custcard});
                cardId = custcard['id'];
              // Save to business' account
                this.creditcard.expMonth = result.source.card.exp_month;
                this.creditcard.expYear = result.source.card.exp_year;
                this.creditcard.last4 = result.source.card.last4;
                this.creditcard.brand = result.source.card.brand;

                console.log(this.creditcard);
                this.businessSrv.update(this.businessObj.id,
                    {creditCard: this.creditcard, stripeCustomerId: customerId, stripeCardId: cardId})
                .then(() => {
                  this.refreshBizSubjectAndGo('New card added');
                }); // businessUpdate
              }).catch((e)=>{console.warn(e);this.toolbox.hideLoading()});; // subscribe
        }
      });
    });
    /**/
  }// setupPayment()

  dismiss(sent= false) {
    this.modalCtrl.dismiss({
      dismissed: sent
    });
  }

  setCCMonth(evt) {
    this.businessObj.creditCard.expMonth = evt.target.value;
  }
  showTOS() {
    alert('Coming soon');
  }

  async saveCard()
  // tslint:disable-next-line: one-line
  {
    const stripeURL = `https://api.stripe.com/v1/customers/${this.businessObj.stripeCustomerId}/sources/${this.businessObj.stripeCardId}`;

    const params2 = `card[name]=${this.businessObj.creditCard.name}
      &card[exp_month]=${this.businessObj.creditCard.expMonth}
      &card[exp_year]=${this.businessObj.creditCard.expYear}`;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization: 'Bearer ' + environment.stripe.secretKey
      })
    };

    await this.toolbox.showLoading();
    this.http.post(stripeURL, params2, httpOptions)
    .pipe(first())
    .subscribe((h) => {
      console.log('card updated: ', h);
      this.businessSrv.update(this.businessObj.id, {'creditCard.name': this.businessObj.creditCard.name,
        'creditCard.expMonth': this.businessObj.creditCard.expMonth,
        'creditCard.expYear': this.businessObj.creditCard.expYear})
          .then(() => {
            this.refreshBizSubjectAndGo('Card details updated');
          })
          .catch((e) => {
            this.toolbox.hideLoading();
            console.log(e);
            this.toolbox.showAlert('Error', e);
          });
    });
  }


  async refreshBizSubjectAndGo(toastMessage: string, dismiss= true) {
    // await this.toolbox.refreshLocalStorage('businesses', 'business', this.businessObj.id)
    await this.businessSrv.refreshBusiness()
    .then(() => {
      this.toolbox.hideLoading();
      this.toolbox.showToast(toastMessage);
      this.dismiss(dismiss);
    }).catch((e) => {this.toolbox.hideLoading();console.warn(e);});
  }

  async deleteCard() {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are you sure you want to delete this card? It cannot be undone.',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            this.toolbox.showLoading();
            this.http.post(
              environment.stripe.deleteCardURL, {
              customerId: this.businessObj.stripeCustomerId,
              cardId: this.businessObj.stripeCardId,
              })
            .subscribe(data => {
              console.log({data});
              this.businessSrv.update(this.businessObj.id, {creditCard: null, stripeCardId: null})
                  .then(() => {
                    this.toolbox.hideLoading();
                    this.refreshBizSubjectAndGo('Card deleted', false);
                  });
            });
          }
        }
      ]
    });
    await alert.present();
  }
}
