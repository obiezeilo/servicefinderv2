import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EmailRegisterPage } from './email-register.page';

describe('EmailRegisterPage', () => {
  let component: EmailRegisterPage;
  let fixture: ComponentFixture<EmailRegisterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailRegisterPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EmailRegisterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
