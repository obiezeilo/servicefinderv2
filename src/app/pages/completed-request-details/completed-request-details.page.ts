import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {ToolboxService } from '../../services/toolbox.service';
import { Quote } from '@models/quote';
import { Business } from '@models/business';
import { ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { Location } from '@angular/common';
import { AuthenticationService } from '@auth/authentication.service';
import { Router } from '@angular/router';
import { UserService } from '@services/user.service';

@Component({
  selector: 'app-request-details',
  templateUrl: './completed-request-details.page.html',
  styleUrls: ['./completed-request-details.page.scss'],
})
export class CompletedRequestDetailsPage implements OnInit {
  reqId: string;
  request: any;
  quotes: Quote[] = [];
  business: Business = new Business();
  quote: Quote;
  quoteSubscription: Subscription;

  constructor(public userSrv: UserService, private route: ActivatedRoute, public location:Location
    , public auth: AuthenticationService, public router: Router) {
    }

  ngOnInit() {
    this.reqId = this.route.snapshot.paramMap.get('id');
    this.route.queryParams.subscribe(params => {
      console.log(JSON.parse(params.obj));
      this.request = JSON.parse(params.obj).req;
    })
  }

  goBack()
  {
    (this.auth.isGuest) ? this.router.navigate(['start']) : this.router.navigate(['completed-requests']);
  }
}
