import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusinessSendQuotePage } from './business-send-quote.page';

const routes: Routes = [
  {
    path: '',
    component: BusinessSendQuotePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusinessSendQuotePageRoutingModule {}
