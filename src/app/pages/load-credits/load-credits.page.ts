import { Component, OnInit } from '@angular/core';
import { Stripe } from '@ionic-native/stripe/ngx';
import { environment } from 'src/environments/environment';
import { ToolboxService } from '@services/toolbox.service';
import { Business } from '@models/business';
import { BusinessService } from '@services/business.service';
import { FirebaseService } from '@services/firebase.service';
import { ModalController } from '@ionic/angular';
import { CreditCardFormPage } from '../credit-card-form/credit-card-form.page';
import { HttpClient } from '@angular/common/http';
import { typeWithParameters } from '@angular/compiler/src/render3/util';
import { PaystackOptions } from 'angular4-paystack';
import { PayWithPaystackPage } from '@pages/pay-with-paystack/pay-with-paystack.page';
import { EmailService } from '@services/email.service';
import { UserService } from '@services/user.service';

@Component({
  selector: 'app-load-credit',
  templateUrl: './load-credits.page.html',
  styleUrls: ['./load-credits.page.scss'],
})
export class LoadCreditsPage implements OnInit {
  cardDetails: any;
  redits: any;
  arrCredits: any;
  businessObj: Business;
  message: string;
  creditcard: {};
  chosencredits = 0;
  chosenbonus = 0;
  chosenobject: any;
  userObj: any;
  payprocessor = 'stripe';
  totalcredits = 100;
  totalcost = 0;
  reference: string;
  public creditcost: any;

  constructor(private stripe: Stripe, private toolbox: ToolboxService, private email: EmailService
    ,public businessSrv: BusinessService, private firebaseSrv: FirebaseService
    ,public modalController: ModalController, private http: HttpClient
    ,private userSrv: UserService) { }

  ngOnInit() {
    this.getInfo();
  }

  async getInfo()
  {
    if (!this.userSrv.getUser()) await this.userSrv.refreshUserSubject();
    this.reference = `ref-${Math.ceil(Math.random() * 10e13)}`;
    let baseamount = 0;
    this.userObj = this.userSrv.getUser();
    this.businessObj = this.businessSrv.getBusiness();// this .toolbox.getFromLocal('business').then((u) => u);
    console.log(this.userObj);

    this.creditcost = await this.toolbox.getJSONFromURL('creditcosts.php?c=' + this.userObj.countrycode);

    console.log('creditcost',this.creditcost);
    this.arrCredits = [];
    let x = 0;
    for (const i in this.creditcost.count) { 
      const quantity = parseInt(i) / this.creditcost.unitcost;
      this.arrCredits.push({key: parseInt(i), quantity, bonus: this.creditcost.count[i].bonus});
      if (x == 0) baseamount = parseInt(i);
      x++;
    }
    console.log(this.arrCredits);
      // Get the payprocessor
      // const countryInfo = await this.firebaseSrv.getById('countries',this.userObj.countrycode);
      this.payprocessor= this.creditcost.payprocessor; //(await this.toolbox.getJSONFromURL('creditcosts.php?c=' + this.userObj.countrycode)).payprocessor; //countryInfo.payprocessor;
      console.log('payprocessor',this.payprocessor);
  }

  payWithStripe(token) {

    /*
    this.stripe.setPublishableKey(environment.stripe.publishableKey);

    this.cardDetails = {
      number: '4242424242424242',
      expMonth: 12,
      expYear: 2020,
      cvc: '220'
    }

    this.stripe.createCardToken(this.cardDetails)
      .then(token => {
        console.log(token);
        //this.makePayment(token.id);
      })
      .catch(error => console.error(error));
      */
  }

  async choosecredits(c) { 
    this.chosenobject = c;
    this.chosencredits = c.quantity;
    this.chosenbonus = c.bonus;
    this.totalcredits = ((this.businessObj.credits) ? this.businessObj.credits : 0) + parseInt(c.quantity + c.bonus);
    this.totalcost = c.key;
    this.message = parseInt(c.quantity + c.bonus) + ' credits will be added to your account';
    this.message += ', and ' + this.userObj.currencySymbol + c.key + ' will be charged to your credit card.';
    this.message += '\r\nA receipt will be sent to ' + this.businessObj.email;
  }

  async openCCForm() {
   /* this.toolbox.goto('credit-card-form');return false;
    */
    const modal = await this.modalController.create({
      component: CreditCardFormPage,
      componentProps: {businessObj: this.businessObj, name: this.userObj.name}
    });
    modal.onDidDismiss().then(async (obj) => {
      if (obj.data.dismissed == true) {
        await this.businessSrv.refreshBusiness().then(()=>{this.businessObj = this.businessSrv.getBusiness();})
        
        /* this.toolbox.refreshLocalStorage('businesses', 'business', this.businessObj.id).then(async () => {
          this.businessObj = await this.toolbox.getFromLocal('business');
          console.log('business refreshed');
        }); */
      } else {
      console.log('cancelled credit card form');
      }
    });
    return await modal.present();
  }

  async payForCredits() {
    console.log(this.totalcost * 100);
    console.log(environment.stripe.chargeURL);

    try{
      this.toolbox.showLoading();
      this.http.post(
        environment.stripe.chargeURL, {
          amount: this.totalcost * 100,
          action:'charge',
          currency: 'usd',
          customerId: this.businessObj.stripeCustomerId,
          description: this.chosencredits + ' credits purchased by ' + this.businessObj.name + ' (' + this.businessObj.phone + ')',
          email: this.businessObj.email
        })
      .subscribe(data => {
        this.completePayment(data);
      });
    }catch(e){
      console.warn(e);
      this.toolbox.hideLoading()
    }
    
  }

  completePayment(data) {
    console.log({data});
    if (data.status != 'succeeded' && data.status != 'success') {
      let errMsg = 'There was an error loading the credits';
      if (data.raw) {
        if (data['raw'].message) { errMsg=data["raw"].message; }
        else if (data['message']) { 
          errMsg=data["message"];
        }
      }

      this.toolbox.showAlert('Error', errMsg);
      console.warn('Error logging: ', data);
      this.toolbox.hideLoading();
      return false;
    }

    let chargeID = '';
    if ( data.id != undefined) {
      chargeID = data.id;
    } else if (data.reference != undefined) {
      chargeID = data.reference;
 }

    let receiptURL = '';
    if (data.receipt_url != undefined) {
      receiptURL = data['receipt_url'];
    } else if (data.redirecturl != undefined) {
      receiptURL = data['redirecturl'];
 }

    const log = {chargeId: chargeID, quantity: (this.chosencredits + this.chosenbonus)
          , cost: this.totalcost, createdDate: Date.now(), receiptURL};
    console.log(log);
    this.businessSrv.update(this.businessObj.id, {credits: this.totalcredits})
    .then(() => {
      return this.businessSrv.addCreditLog(this.businessObj.id, log);
    })
    .then(() => {
      // Email the receipt
      console.log('log updated');
      this.toolbox.showToast('Credit loaded');
      return this.businessSrv.refreshBusiness(); //this.toolbox.refreshLocalStorage('businesses', 'business', this.businessObj.id);
    })
    .then(() => {
      this.toolbox.hideLoading();
      console.log('business refreshed.');
      this.toolbox.goto('business-account', null, true);
    })
    .catch((e) => {
      this.toolbox.hideLoading();
      this.toolbox.showAlert('Error', e);
      console.warn('Error logging: ', e);
    });
  }

  // Pay by Paystack
  async openPaystackForm() {
    const modal = await this.modalController.create({
      component: PayWithPaystackPage,
      componentProps: {businessEmail: this.businessObj.email, totalcost: this.totalcost}
    });
    modal.onDidDismiss().then((obj) => {
      console.log(obj);
      if (obj.data.dismissed == false && obj.data.info != undefined) {
        this.toolbox.showLoading();
        this.completePayment(obj.data.info);
      } else {
      console.log('cancelled credit card form');
      }
    });
    return await modal.present();
  }

  viewHistory() {
    this.toolbox.goto('credit-history', {businessObj: this.businessObj});
  }
}
