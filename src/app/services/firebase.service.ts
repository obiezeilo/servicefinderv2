import { Injectable } from '@angular/core';
import 'firebase/firestore';
import 'firebase/auth';
import { arrayUnion, orderBy, setDoc, collectionData, QueryConstraint, WhereFilterOp, Query, DocumentData, docData } from '@angular/fire/firestore';
import { doc, addDoc, getDoc, getDocs,updateDoc,deleteDoc,query,where,
  collection, Firestore } from'@angular/fire/firestore';
import { AuthenticationService } from '../authentication/authentication.service';
import { Observable } from 'rxjs';
import { getStorage, ref, getDownloadURL, deleteObject, uploadString } from 'firebase/storage';

// import { AngularFirestore, Query } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  constructor(private readonly auth: AuthenticationService, private readonly firestore: Firestore)
  { }

  async getById(_collection:string, id:string)
  {
    const snapshot = await getDoc(doc(this.firestore, _collection, id));
    const data = snapshot.data() as object;
    return (snapshot.exists()) ? {id,...snapshot.data()} as any : null;
  }

  getById$(_collection:string, id:string): Observable<any>
  {
   const noteDocRef = doc(this.firestore, `${_collection}/${id}`);
    return docData(noteDocRef, { idField: 'id' }) as Observable<any>;
  }


  async get(_collection, filter?:Array<any>, arr?:Array<string>, order?:string)
  {
    let q = query(collection(this.firestore, _collection));

    const queryConstraints: QueryConstraint[] = [];
    if (filter)
    {
      filter.forEach(e => {
        const flt=e.split('|');
        if (arr) flt[2]=arr;
        queryConstraints.push(where(flt[0],flt[1],flt[2]))
      });
    }
    if (order != undefined) queryConstraints.push(orderBy(order, 'desc'))
    q = query(collection(this.firestore, _collection), ...queryConstraints);

    const querySnapshot = await getDocs(q);
    return querySnapshot.docs.map(_doc => {
      const data = _doc.data() as object;
      const id = _doc.id;
      return {id,...data} as any;
    });
  }

  get$(_collection, filter?:FirestoreQuery[], arr?:Array<string>, order?:string[]): Observable<any[]>
  {
    const queryConditions: QueryConstraint[] = filter.map((condition) =>
      where(condition.property, condition.operator, condition.value));
    if (order !== undefined)
      order.forEach((o)=>{
        queryConditions.push(orderBy(o, 'desc'))
      })

    const qry: Query<DocumentData> = query(collection(this.firestore, _collection), ...queryConditions)
    return collectionData(qry, {idField:'id'});
  }


  add(_collection:string,item, id?:string)
  {
      let addPrm: Promise<any>;
      if (id)
      {
        addPrm = setDoc(doc(this.firestore, _collection, id), JSON.parse(JSON.stringify(item)))
      }
      else
      {
        addPrm = addDoc(collection(this.firestore, _collection), JSON.parse(JSON.stringify(item)))
      }

      return addPrm.then(
        res => {return (res)},
        err => {return (err)}
      )
  }

  update(_collection, id: string, item): Promise<void>
  {
    return updateDoc(doc(this.firestore, _collection, id),item);
  }

  updateArray(_collection:string, id: string, fieldname:string, array:any)
  {
    return updateDoc(doc(this.firestore, _collection, id), {[fieldname]: arrayUnion(Object.assign({}, array))});
  }

  delete(_collection, id: string): Promise<void> {
    return deleteDoc(doc(this.firestore, _collection, id));
  }

  async uploadFile(file: any, location: string)
  {
    const storage = getStorage();
    const imageRef = ref(storage, location);
    const _file = file.replace('data:image/jpeg;base64,','').replace('data:image/png;base64,','');
    return await uploadString(imageRef, _file, 'base64')
      .then(async (snapshot) => {
        console.log('Uploaded');
        console.log('File metadata:', snapshot.metadata);
        // Let's get a download URL for the file.
        const url = await getDownloadURL(snapshot.ref);
        console.log('url', url);
        return url;
      }).catch((error) => {
        console.error('Upload failed', error);
        return error;
        // ...
      });
  }

  async deleteFile(location)
  {
    const storage = getStorage();

    // Create a reference to the file to delete
    const desertRef = ref(storage, location);

    // Delete the file
    deleteObject(desertRef).then(() => {
      console.log(`deleted ${location}`);
      // File deleted successfully
    }).catch((error) => {
      // Uh-oh, an error occurred!
      console.warn(`error deleting: ${error}`);
    });
  }
}

export interface FirestoreQuery
{
  property: string;
  operator: WhereFilterOp;
  value: unknown;
}