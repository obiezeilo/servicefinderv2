
export class Business {
    id?: string;
    name: string;
    category: string[];
    address?: string;
    city: string;
    state:string;
    zip?:string;
    country:string;
    email:string;
    phone:string;
    website?:string;
    logo?:string;
    userId:string;
    rating:number;
    ratingCount:number;
    latitude:number;
    longitude:number;
    credits:number;
    isActive:boolean;
    deleted?:boolean;
    deviceToken:string;
    creditCard:{last4:number; expMonth:number;expYear:number; ccv:number; name:string; brand:string; token:string};
    creditLoadHistory:{}
    stripeCardId:string;
    stripeCustomerId:string;
    stripeClientSecret:string;
  }