import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {RequestService} from '../../services/request.service';
import Request from '../../models/request';
import { BehaviorSubject } from 'rxjs';
import {ToolboxService } from '../../services/toolbox.service';
import { UserService } from '@services/user.service';
import { AuthenticationService } from '@auth/authentication.service';

@Component({
  selector: 'app-completed-requests',
  templateUrl: './completed-requests.page.html',
  styleUrls: ['./completed-requests.page.scss'],
})
export class CompletedRequestsPage implements OnInit {
  requests$: BehaviorSubject<Request[]>;
  public noRequests = true;

  constructor( private router: Router, public userSrv: UserService,
               public tools: ToolboxService,
               private authService: AuthenticationService, private reqService: RequestService
  ) {console.log('home constructor'); }

  ngOnInit() {
    this.getRequests();
  }

  async getRequests()
  {console.log('getting');
    if (!this.authService.getUser) await this.userSrv.refreshUserSubject();  // to load the user into the subject for subsequent pages
    
    const usr = this.authService.getUser;
    console.log('inside getRequests: ', usr)
    const arr = [];
    arr.push({property:'status', operator:'==', value:'completed'});
    arr.push({property:'userId', operator:'==', value:usr.uid});

    this.requests$ = this.reqService.getSub$(arr);
    this.requests$.subscribe((reqs) => {
      if (reqs)
      { this.noRequests=false;
        reqs.map((req)=>{
          req['timeago'] = (req.status === 'new') ? this.tools.timeAgo(req.createdDate) : this.tools.timeAgo(req.statusDate);
        })
      }
    });
  }

  ionViewDidEnter() {
    console.log('home ionViewDidEnter');console.log(this.authService.getUser);
  }

  async gotoDetails(req)
  { const obj = JSON.stringify({req});
    await this.router.navigate(['completed-request-details', req.id], {queryParams: {obj}});
  }

  ngOnDestroy() {
    console.log('home ondestroy');
  }
}
