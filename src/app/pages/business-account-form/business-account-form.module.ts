import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusinessAccountFormPageRoutingModule } from './business-account-form-routing.module';

import { BusinessAccountFormPage } from './business-account-form.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusinessAccountFormPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [BusinessAccountFormPage]
})
export class BusinessAccountFormPageModule {}
