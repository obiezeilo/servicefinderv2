import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AuthenticateService } from '@services/auth.service';
import { ToolboxService } from '@services/toolbox.service';
import { BusinessService } from '@services/business.service';
import { take } from 'rxjs/operators';
import { UserService } from '@services/user.service';
// import { FcmService } from '@services/fcm.service';

@Component({
  selector: 'app-email-login',
  templateUrl: './email-login.page.html',
  styleUrls: ['./email-login.page.scss'],
})
export class EmailLoginPage implements OnInit {
  constructor(

    private navCtrl: NavController,
    private authService: AuthenticateService,
    private formBuilder: FormBuilder,
    private tools: ToolboxService,
    private businessSrv: BusinessService,
    private userSrv: UserService,
    private router: Router

  ) {
  //  console.log = function(){}
  }

  validations_form: FormGroup;
  errorMessage = '';
  token:string;


  validation_messages = {
    email: [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Please enter a valid email.' }
    ],
    password: [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 5 characters long.' }
    ]
  };

  ngOnInit() {
    this.validations_form = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
    });
  }


  async loginUser(value){
    console.log(value);
    const theUser = await this.authService.loginUser(value);
    console.log(theUser);

    if(theUser.message !='' && theUser.message != undefined){
      this.tools.showAlert('Error',theUser.message);
      return false;
    }

    if(this.token === undefined){
      this.token = theUser.refreshToken;
    }
    // Update the devicetoken in FB
    console.log('Updating the devicetoken',this.token);
    await this.userSrv.update(theUser.uid,{deviceToken:this.token});

    // Refresh the local storage
    console.log('Refresh the local storage',this.token);
    await this.tools.refreshLocalStorage('users','user',theUser.uid)

    // Get business data, if any
    console.log('getting business from ', theUser.uid)
    const arr=[]
    arr.push('userId|==|' + theUser.uid);

    const biz = await this.businessSrv.get(arr).then((b)=>{return b[0]});

    if (biz)
    { console.log('business found', biz);
      // update the deviceToken
      await this.businessSrv.update(biz.id,{deviceToken:this.token})
      await this.tools.refreshLocalStorage('businesses','business',biz.id)

    }
    else {console.log('No business account'); return true}

    this.router.navigate(['tabs/home']);
  }

  goToRegisterPage(){
    this.router.navigate(['/email-register']);
  }

  goToforgotpassword(){
    this.router.navigate(['/forgotpassword']);
  }
}