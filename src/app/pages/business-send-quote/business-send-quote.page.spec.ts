import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BusinessSendQuotePage } from './business-send-quote.page';

describe('BusinessSendQuotePage', () => {
  let component: BusinessSendQuotePage;
  let fixture: ComponentFixture<BusinessSendQuotePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessSendQuotePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BusinessSendQuotePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
