import { Injectable } from '@angular/core';
import {
  Auth,
  createUserWithEmailAndPassword,
  EmailAuthProvider,
  linkWithCredential,
  sendPasswordResetEmail, signInAnonymously,
  signInWithEmailAndPassword, signOut,
  User,
  } from '@angular/fire/auth';
import { Observable, of } from 'rxjs';
import { StorageService } from '@services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private storage: StorageService
    , private readonly auth: Auth) 
  {
    console.log('curUser:', this.auth.currentUser)
  }

  async registerUser(value)
  {
     return await createUserWithEmailAndPassword(this.auth, value.email, value.password)
     .then(res => {return res.user;})
     .catch(err => { return err })
  }

  async signIn(value)
  {
    return (await signInWithEmailAndPassword(this.auth, value.email, value.password)).user
  }

  /** Log in anonymously */
  async loginAsGuest(){
    return await signInAnonymously(this.auth)
      .then(res => {return res.user;})
      .catch(err => { console.warn('error guestlogin:', err); return err; })
   }

  async logOut(): Promise<void>{
    // preserve the token when you log out
    const token = await this.storage.get('deviceToken');
    await this.storage.clear()
    await this.storage.set('deviceToken', token);

    signOut(this.auth)
      .then(() => { return (true);})
      .catch((error) => { console.warn(error); return(error); });
  }

  /** Get the authenticated user */
  get getUser(): User {
    return this.auth.currentUser;
  }

  /** Get the authenticated user as an observable */
  getUser$(): Observable<User>
  { return of(this.getUser);
   }

  forgotpassword(email:string){
    return sendPasswordResetEmail(this.auth, email);
  }

  get isGuest()
  {
    return this.auth.currentUser.isAnonymous;
  }

  async convertGuestToAccount(email: string,password: string)
  {
    const credential = EmailAuthProvider.credential(email, password);

    return linkWithCredential(this.auth.currentUser, credential)
      .then((usercred) => {
        const user = usercred.user;
        console.log('Anonymous account successfully upgraded', user);
        return user;
      }).catch((error: any) => {
        console.log('Error upgrading anonymous account', error);
        return error;
      });
  }
}
