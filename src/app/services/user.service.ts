import { Injectable } from '@angular/core';
import { FirebaseService } from '@services/firebase.service';
import { UserModel } from '@models/user';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthenticationService } from '@auth/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public userSubject$ = new BehaviorSubject<UserModel | null>(null);

  constructor(private fbServ: FirebaseService, private authService: AuthenticationService)
  {
    this.refreshUserSubject();
  }


  async refreshUserSubject():Promise<UserModel>
  {
    const u = this.authService.getUser;
    if (!u) return null;console.log(u);

    return await this.fbServ.getById('users', u.uid)
      .then((usr:UserModel)=>{
        if (usr === undefined) return null;
        this.userSubject$.next(usr);
        console.log('retrieved user: ',usr);
        return usr;
      });
  }

  /** Return the current user as an observable
   * @author Obinna Ezeilo
   * @returns An observable of the user
   */
  watchUser(): Observable<UserModel | null>
  {
    return this.userSubject$;
  }

  /** Return the current user subject */
  getUser(): UserModel | null
  {
    return this.userSubject$.value;
  }

  /** Update the user subject */
  updateUserObj(u: UserModel)
  {
    this.userSubject$.next(u);
  }

  async add(obj:UserModel)
  {
    return await this.fbServ.add('users',obj, obj.id);
  }

  async update(id:string,obj)
  {
    return await this.fbServ.update('users',id,obj);
  }

  /**
   * Get list of users based on the filter
   * @param filter 
   * @returns array of users
   */
  get(filter:Array<string>)
  {
    return this.fbServ.get('users', filter);
  }

  /**
   * 
   * @param id Get a single user based on the ID
   * @returns 
   */
  async getById(id)
  {
    return await this.fbServ.getById('users', id);
  }

  async getById$(id)
  {
    return await this.fbServ.getById$('users', id);
  }
}
