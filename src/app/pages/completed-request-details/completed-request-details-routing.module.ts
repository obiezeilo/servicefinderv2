import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CompletedRequestDetailsPage } from './completed-request-details.page';

const routes: Routes = [
  {
    path: '',
    component: CompletedRequestDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CompletedRequestDetailsPageRoutingModule {}
