import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoadCreditsPage } from './load-credits.page';

const routes: Routes = [
  {
    path: '',
    component: LoadCreditsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoadCreditsPageRoutingModule {}
