import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoadCreditsPageRoutingModule } from './load-credits-routing.module';

import { LoadCreditsPage } from './load-credits.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoadCreditsPageRoutingModule
  ],
  declarations: [LoadCreditsPage]
})
export class LoadCreditsPageModule {}
