import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Quote } from '@models/quote';
import Request from '@models/request';
import {ToolboxService} from '@services/toolbox.service';
import { ModalController } from '@ionic/angular';
import {BusinessService} from '@services/business.service';
import { Business } from '@models/business';
import { RatingPage } from '@pages/rating/rating.page';
import { AuthenticationService } from '../../authentication/authentication.service';


@Component({
  selector: 'app-quote-details',
  templateUrl: './quote-details.page.html',
  styleUrls: ['./quote-details.page.scss'],
})
export class QuoteDetailsPage implements OnInit {
  quote: Quote = new Quote();
  request: Request = new Request();
  businessObj: Business;
  businessRating: any;
  currency: string;

  constructor(private route: ActivatedRoute, private router: Router,
              private businessSrv: BusinessService, private auth: AuthenticationService
    ,         private tools: ToolboxService, private modalCtrl: ModalController) {  }

  ngOnInit() {
    this.setCurrency();
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        console.log(this.router.getCurrentNavigation().extras.state.params);
        this.request = this.router.getCurrentNavigation().extras.state.params.request; 
        this.quote = this.router.getCurrentNavigation().extras.state.params.quote;
        console.log(this.quote);
        this.businessSrv.getById(this.quote.businessId).then((b: Business) => {
          this.businessObj = b;
          this.businessRating = (this.businessObj.rating == 0 ? 'Unrated' : Math.ceil(this.businessObj.rating / this.businessObj.ratingCount) + '/5');
        });
      }
    });
  }

  async setCurrency()
  {
    this.currency = (await this.tools.getFromLocal('countryInfo')).currencysymbol;
  }
  backToRequest() {
    this.tools.goto('request-details', {request: this.request});
  }

  call() {
    window.open('tel:' + this.businessObj.phone);
  }
  sendMessage() {
    console.log(this.request, this.quote)
    this.tools.goto('messages', {requestId: this.request.id, businessId: this.quote.businessId});
  }

// Rate the service
  async rate() {
    const modal = await this.modalCtrl.create({
      component: RatingPage,
      componentProps: {request: this.request, quote: this.quote, business: this.businessObj, requestId: this.quote.requestId}
    });
    modal.onDidDismiss().then((obj) => {
      if (obj.data.dismissed === true)
      { if (obj.data.rating)
        {
          this.quote.rating = obj.data.rating;
          if (obj.data.ratingNotes) { this.quote.ratingNotes = obj.data.ratingNotes; }
          if (this.auth.isGuest)
          {
            const reqObj = JSON.stringify({req:this.request});
            this.router.navigate(['completed-request-details', this.request.id], {queryParams: {obj:reqObj}});
          }
          else
            this.tools.goto('/tabs/completed-requests');
        }
      } else {
        console.log('cancelled sending quote');
      }
    });
    return await modal.present();
  }

  showHowLongAgo(tstamp)
  {
    return this.tools.timeAgo(tstamp);
  }
}
