//import { stringify } from 'querystring';

const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const admin = require('firebase-admin');
admin.initializeApp();
const GeoFire = require("geofire");
const default_search_radius = 65;
const db = admin.firestore();
//const cors = require('cors')({origin: true});

//admin.initializeApp(functions.config().firebase);

// Notify on new request
exports.notifyNewRequest = functions.database.ref('/requests/{requestId}').onCreate((data, context) =>
{   
    // This should rarely occur, but it's just a precaution
    try {
        if (!data.val()) {console.log('No request object');  return false; }
        if (data.val().status !== 'new') {console.log('not a new one.');  return false;}// shouldn't be needed, since new requests have a 'new' status
    } catch (error) {
        console.log("data() error: ", error);
        console.log(data);
        return false;
    }
    
    const req = data.val();

    let deviceTokens = [];

    /*** find them via GeoFire */
    var latlng = [parseFloat(req.latitude), parseFloat(req.longitude)];
    const geofire = new GeoFire(admin.database().ref('businesslocations'));
    var businessesnotified=0;
    var geoQuery = geofire.query({
        center: latlng,
        radius: default_search_radius
      });
      
    try {
        geoQuery.on("key_entered", (key, location, distance) =>{
            //console.log(key + " entered query at " + location + " (" + distance + " km from center)");
            
            admin.database().ref('businessprofiles/' + key).once('value',(snapshot)=>{
                //console.log(snapshot);
                if (!snapshot.val().status)
                {
                    console.log("inactive business skipped: ",snapshot.key);
                    return false;
                }
                if (snapshot.val().category === req.category)// If the category matches, get the token
                {   console.log("categories match. Going to 'users/" + key)
                    // Get token
                    var dataParam = 
                    {   page:'BusinessOverviewPage', 
                        tab:'new',
                        user_id:req.userid, 
                        category:req.category, 
                        title:'New Request', 
                        body:'You have a new ' + req.category + ' request from ' + req.city
                    }
                    sendNotification(key,'New request', req.category + ' request from ' + req.city,dataParam );
                    businessesnotified += 1;
                    
                    // Update the count to show on the app
                    admin.database().ref('requests/' + data.key + '/businessesnotified').set(businessesnotified);
                    
                    return null;
                }
                else
                {
                    console.log("No match: snapshot.category='" + snapshot.val().category + "'; req.category='" + req.category + "'");
                }
                return null;
            });
        });
    } catch (error) {
        console.log("Geoquery error: ", error);
    }
    /*** /Geofire */

    return true;
})//onWrite


/******** Notify on new, rejected, refunding or accepted quote ***************/
exports.notifyQuote = functions.database.ref('/quotes/{quoteId}/').onWrite((change, context) =>
{
    // This should rarely occur, but it's just a precaution
    if (!change.after.val()) {console.log('No quote object');  return false;}

    const quote = change.after.val();
    
    var title, message, token, userid=null, userid_with_token, redirPage='BusinessOverviewPage'
        , redirTab="quoted",page='TabsHome',tab='1', amount = (quote.currency_symbol) ? quote.currency_symbol : "";
    amount += quote.amount;

    switch (quote.status) {
        case 'new':
        case null:
        case '':
            title = 'New quote';
            message = 'You have a new quote of ' + numberWithCommas(amount) + ' from ' + quote.businessprofile_name;
            redirPage = 'TabsHome'
            redirTab = "1";
            // Update the quote count
            admin.database().ref('quotes').orderByChild("request_id").equalTo(quote.request_id).once('value',(snapshot)=>{
                console.log("quote count is ",snapshot.numChildren());
                admin.database().ref('requests/' + quote.request_id + "/quote_count").set(snapshot.numChildren()
                    ,(s)=>{ console.log("quote count updated to " + snapshot.numChildren() + ". Returned ",JSON.stringify(s));}
                );
            });
            break;

        case 'accepted':
            title = 'Quote accepted';
            message = 'Your quote of ' + numberWithCommas(amount) + ' has been accepted.';
            tab = '2';
            userid=quote.businessprofile_id;
        	page='BusinessOverviewPage';
            break;

        case 'rejected':
            userid=quote.businessprofile_id;
            title = 'Quote rejected';
            message = 'Your quote of ' + numberWithCommas(amount) + ' was rejected.';
        	tab = '1';	
        	page='BusinessOverviewPage';
            break;

        default:
            break;
    }
    

    if (userid === null)//get the userID first
    {
        admin.database().ref('requests/' + quote.request_id + "/userid").once('value',(usersnapshot)=>{
            sendNotification(usersnapshot.val(),title,message, {page:page, tab:tab, bizprofile_id:quote.businessprofile_id, title:title, body:message});
        });
    }
    else
    {
        sendNotification(userid,title,message, {page:page, tab:tab, bizprofile_id:quote.businessprofile_id, title:title, body:message});
    }

    return null;
})//onWrite



// Notify on new message
exports.notifyMessage = functions.database.ref('/messages/{requestId}').onWrite((data, context)  =>
    {
        // This should rarely occur, but it's just a precaution
        if (!data.after.val()) {console.log('No message was sent');  return false;    }
  		//console.log("Before: " + Object.keys(data.before.val().chats).length + "; after: " + Object.keys(data.after.val().chats).length);
        if (data.before.val())
        {
  			if (Object.keys(data.before.val().chats).length === Object.keys(data.after.val().chats).length)
            {	console.log('No new chats. Exiting notifyMessage'); 
             	return false;
            }
  		}
  		else console.log('No past chats')
        
        // IF no new chat was created (that is, if all chats were set to 'read' and no new ones added), ignore it
        const msg = data.after.val();
        
        
        // Get the device token path
        var fromwho="", params="", notifyuserid;
          
        var lastchat, unreadcount=0;
        for (c in msg.chats)
        {   
            if (msg.chats[c].read === false) unreadcount++;
          	lastchat = msg.chats[c]; 
        }

        if ((lastchat.sender === 'user'))
        {   notifyuserid = msg.businessprofile_userid;
            fromwho = msg.username;
            params = {page:'BusinessOverviewPage', whichtab:'new', user_id:msg.businessprofile_userid, bizprofile_id:msg.businessprofile_userid,
                title: 'New message',body: 'You have a new message from ' + msg.username
            };
        }
        else if ((lastchat.sender === 'business'))
        {   notifyuserid = msg.userid;
            fromwho = msg.businessname;
            params = {page:'MessagesListPage', tab:'new', user_id:msg.userid, bizprofile_id:msg.businessprofile_userid,
            title: 'New message',body: 'You have a new message from ' + msg.businessname};
        }
        
        sendNotification(notifyuserid,'New message', 'You have a new message from ' + fromwho, params);
        console.log("Sending to ", notifyuserid);

        return null;

    })//onWrite

// Notify on rated service
exports.notifyRatedService = functions.database.ref('/requests/{requestId}').onWrite((change, context) =>
    {
        // This should rarely occur, but it's just a precaution
        if (!change.after.val()) {console.log('No request object');  return false;  }
    
        // Only called for rated services
        if ((change.after.val().status !== 'completed') || (!change.after.val().ratings_value))
        {   console.log('No need.');
            return false;
        } 
    
        const req = change.after.val();
        
        // send to the device
        console.log("Sending to device ", req.quote.businessprofile_id);
        
        sendNotification(req.quote.businessprofile_id,'Service rated',
            'Your service was rated ' + req.ratings_value + '/5.',
                {page:'BusinessOverviewPage', tab:'completed',user_id:req.userid, bizprofile_id:req.accepted_businessprofile_id,
                title: 'Service rated',body: 'Your service was just rated ' + req.ratings_value + '/5.'});
            return null;
        
    })//onWrite
    

exports.updateLocation = functions.database.ref('/businessprofiles/{id}').onWrite((change, context) => 
{
    // This should rarely occur, but it's just a precaution
    if (!change.after.val()) {console.log('No biz object');  return false;}

    var biz = change.after.val();
    console.log("The id=", context.params.id)
    
    var latlng = [parseFloat(biz.latitude),parseFloat(biz.longitude)];
    
    const geofire = new GeoFire(admin.database().ref('businesslocations'));
    if (biz.status===true && !biz.deleted)
    {
        geofire.set(context.params.id, latlng)
            .then((c)=>{console.log("Geofire locations saved");return true;})
            .catch((e)=>{console.log("Geofire save error: ", e)});
    }
    else
    {
        geofire.remove(context.params.id)
            .then((c)=>{console.log("Location removed.");return true;})
            .catch((e)=>{console.log("Geofire remove error: ", e)})
    }
    return true;
})

// After logging in, if the user has an active business account, set its location in GeoFire
exports.setLocationOnLogin = functions.database.ref('/users/{id}/last_login_date').onWrite((change, context) => 
{
    // This should rarely occur, but it's just a precaution
    if (!change.after.val()) {console.log('No date updated');  return false;}

    // get the coordinates
    admin.database().ref('/businessprofiles/' + context.params.id).once('value',(data)=>{
        if (!data.val()) {console.log('No business account'); return false;}
        //console.log(data.val());

        var latlng = [parseFloat(data.val().latitude),parseFloat(data.val().longitude)];
        
        const geofire = new GeoFire(admin.database().ref('businesslocations'));
        if (data.val().status===true && !data.val().deleted)
        {
            geofire.set(context.params.id, latlng)
                .then((c)=>{console.log("Geofire locations saved for ", context.params.id);return true;})
                .catch((e)=>{console.log("Geofire save error: ", e)});
        }
        else
        {
            geofire.remove(context.params.id)
                .then((c)=>{console.log("Location removed.");return true;})
                .catch((e)=>{console.log("Geofire remove error: ", e)})
        }
        return true;
    })
    return true;
})


/*
Send Notifications
*/
function sendNotification(recipients, title, body, objParams)
{
    var payload = {
        notification:{
            title: title,
            body: body/*,
            sound: 'default',
            badge: '1',
            click_action:"FCM_PLUGIN_ACTIVITY"*/
        },
        data: objParams
    }
   
    // Ignore devices that have disabled notifications
    var _recipients = [], _recipientsWithDisabledNotifications=[];   // the recipients excluding those with disabled notifications
    admin.database().ref('/notificationsDisabled').once('value',(disabledtokens)=>{
        if (!Array.isArray(recipients))
            _recipients.push(recipients);
        else
            _recipients = recipients;

        console.log("sendNotification recipients: ", JSON.stringify(_recipients));

        _recipients.forEach(r => {
            admin.database().ref('/users/' + r + "/deviceToken").once('value',(dtTokens)=>{
                
                //console.log("token found from "+r+": ", dtTokens.val());
                if (!disabledtokens.hasChild(dtTokens.val()))
                {        
                    admin.messaging().sendToDevice(dtTokens.val(), payload)
                        .then((response)=>
                        {
                            console.log("Notification success: ", JSON.stringify(response));
                            return true;
                        })
                        .catch((error)=>{ console.log("Error sending notification: " + JSON.stringify(error)); console.log("Payload: " + JSON.stringify(payload));
                        });//sendtodevice
                }
                else
                {
                    console.log("disabled? " + dtTokens.val() + "=", JSON.stringify(disabledtokens));
                }
                //snapshot haschild 
            });
        }) // for(var r in _recipients)

        return true;
    });
}

/* Format a number with commas */
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


// Log other updates (play, send, won, lost)
exports.acceptQuote = functions.firestore.document('quotes/{id}')
     .onUpdate((change, context) => {
        
        if (change.after.data().status === change.before.data().status)  return null;

        const newData = change.after.data();
        switch (newData.status) {
            case 'accepted':
                //update the request object, and notify the business
                db.collection("requests").doc(newData.requestId).update(
                    {
                        acceptedQuoteAmount: newData.amount,
                        acceptedQuoteId:id,
                        businessId:newData.businessId
                    }).then((r)=>{
                        // notify the business
                        console.log("Updated quote ", id);
                        return true;
                    }).catch((e)=>{console.log(e); return false;});
                break;
            
            case 'rejected':
                // notify the business
                break;
            case 'new':
                //notify the business
                break;
            default:
                break;
        }
        return true;
      });