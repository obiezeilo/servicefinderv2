import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CompletedRequestsPage } from './completed-requests.page';

const routes: Routes = [
  {
    path: '',
    component: CompletedRequestsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CompletedRequestsPageRoutingModule {}
