import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserService } from '@services/user.service';
import { Device } from '@capacitor/device';
import {ToolboxService } from '@services/toolbox.service';
import { ModalController } from '@ionic/angular';
import { TosPage } from '../tos/tos.page';
import { AuthenticationService } from '../../authentication/authentication.service';
import { App } from '@capacitor/app';
import { BusinessService } from '@services/business.service';


@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit, OnDestroy {

  public appVersion: string;
  public theYear: number;

  constructor(private tools: ToolboxService,
              public userSrv: UserService,
              public modalCtrl: ModalController,
              public authService: AuthenticationService,
              private businessSrv: BusinessService)
  {  }
  

  ngOnInit() {
    console.log('business obj to clear', this.businessSrv.businessSubject$.value);
    Device.getInfo().then(res => {
      if (res.platform !== 'web') {
        App.getInfo().then((a)=>this.appVersion = 'v' + a.version);
      }
    });
    this.theYear = new Date().getFullYear();
    
  }

  goTo(page, params = null) {
    this.tools.goto('/' + page, params);
  }

  async openTOS() {
    const modal = await this.modalCtrl.create({
      component: TosPage,
      componentProps: {lang: 'en'}
    });
    await modal.present();
  }

  async logOut()
  { console.log('logging out')
    this.userSrv.updateUserObj(null);
    this.businessSrv.destroySubject();console.log('cleared biz', this.businessSrv.businessSubject$.value)
    await this.authService.logOut();
  }
  
  comingSoon() {
    this.tools.showAlert('Coming soon', 'This feature is coming soon');
  }

  ngOnDestroy(): void {
  //  this.userSrv.userSubject$ = null;
  }
}