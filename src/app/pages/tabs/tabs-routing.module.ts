import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children:
      [
        {
          path: 'home',
          children:
            [
              {
                path: '',
                loadChildren: ()=>import('../home/home.module').then(m=>m.HomePageModule)
              }
            ]
        },
        {
          path: 'messages-list',
          children:
            [
              {
                path: '',
                loadChildren: ()=>import('../messages-list/messages-list.module').then(m=>m.MessagesListPageModule)
              }
            ]
        },
        {
          path: 'user-profile',
          children:
            [
              {
                path: '',
                loadChildren: ()=>import('../user-profile/user-profile.module').then(m=>m.UserProfilePageModule)
              }
            ]
        },
        {
          path: 'settings',
          children:
            [
              {
                path: '',
                loadChildren: ()=>import('../settings/settings.module').then(m=>m.SettingsPageModule)
              }
            ]
        },
        {
          path: 'business-account',
          children:
            [
              {
                path: '',
                loadChildren: ()=>import('../business-account/business-account.module').then(m=>m.BusinessAccountPageModule)
              }
            ]
        },
        {
          path: 'completed-requests',
          children:
            [
              {
                path: '',
                loadChildren: ()=>import('../completed-requests/completed-requests.module').then(m=>m.CompletedRequestsPageModule)
              }
            ]
        },
        {
          path: 'request-form',
          children:
            [
              {
                path: '',
                loadChildren: ()=>import('../request-form/request-form.module').then(m=>m.RequestFormPageModule)
              }
            ]
        },
        {
          path: '',
          redirectTo: '/tabs/home',
          pathMatch: 'full'
        }
      ]
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
