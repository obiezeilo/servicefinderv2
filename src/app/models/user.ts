export class UserModel {
    id?: string;
    name: string;
    city: string;
    state:string;
    zip?:string;
    country:string;
    countrycode:string;
    email?:string;
    phone?:string;
    latitude:number;
    longitude:number;
    createdDate:number;
    deviceToken:string;
    currencySymbol:string;
    currencyCode?:string;
    guest?:boolean;
  }