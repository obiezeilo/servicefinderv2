import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../authentication/authentication.service';
import { UserModel } from '@models/user';
import { UserService } from '@services/user.service';
import { RequestService } from '@services/request.service';
import { Router, NavigationExtras } from '@angular/router';
import { Geolocation } from '@capacitor/geolocation';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ToolboxService } from '@services/toolbox.service';
import { FirebaseService } from '@services/firebase.service';

@Component({
  selector: 'app-start',
  templateUrl: './start.page.html',
  styleUrls: ['./start.page.scss'],
})
export class StartPage implements OnInit {
  token: string;
  code = ''; // 'YRK5294876236461';
  public showError = false;
  public country = '';
  public state = '';
  public countryInfo:any;
  public env: string;

  constructor(private router: Router,
              private auth: AuthenticationService,
              private userSrv: UserService,
              private http: HttpClient,
              private tools: ToolboxService,
              private fbServ: FirebaseService,
              private requestSrv: RequestService) {}

  ngOnInit() {this.init();}

  
  async init()
  { setTimeout(() => {
    if (this.auth.getUser && !this.auth.isGuest) this.router.navigate(['/tabs/home/']);
    }, 500);
    if(!environment.production) this.env = 'test';
    
    try {   
      this.countryInfo = await this.tools.getFromLocal('countryInfo');
      let _state =  await this.tools.getFromLocal('state');

      if (this.countryInfo && _state)
      { console.log('country & state set');
        this.country=this.countryInfo.countryname;
        this.state=_state;
        return;
      }

      let locstatus:any=null;
      if (navigator.permissions) locstatus=await Geolocation.checkPermissions(); //.requestPermissions();

    // Get the user's location, country info, etc and save to memory environment.
      if (locstatus && locstatus.location != 'denied' || !this.countryInfo)
      {
        const latlng = await Geolocation.getCurrentPosition();

        const geocodingURL=`${environment.geocoding.url}latlng=${latlng.coords.latitude},${latlng.coords.longitude}&location_type=APPROXIMATE&result_type=locality&key=${environment.geocoding.api_key}`;
        console.log({geocodingURL});
        const stateCountry = await this.http.get(geocodingURL).toPromise();

        const addrInfo=stateCountry['results'][0].address_components;
        console.log({addrInfo});

        this.state=addrInfo.filter(a=>a.types[0]=='administrative_area_level_1')[0].long_name;
        this.country=addrInfo.filter(a=>a.types[0]=='country')[0].long_name; 
        const countryabbr=addrInfo.filter(a=>a.types[0]=='country')[0].short_name; 
        
        // get country info
        console.log(this.state,this.country,{countryabbr});

        if (this.countryInfo) 
        {  this.countryInfo = await (await fetch(environment.sfURL + 'countries.php?c=' + countryabbr, {
            method: 'GET',
            mode: 'cors'
          })).json();
        console.log(this.countryInfo, this.state);
        }
      }
        
    } catch (error) {
      console.warn('error:', error);
    }
    // store in the storage
    await this.tools.saveToLocal('countryInfo', this.countryInfo);
    await this.tools.saveToLocal('state', this.state);
  }

  async goToRequest()
  { 
    try {
      const req = await this.requestSrv.getById(this.code.trim());
      if (req)
      {
        if (!this.auth.getUser)
        { console.log('creating guest account');
          await this.loginAsGuest();
        }
        else{
          await this.userSrv.refreshUserSubject();
          console.log('logged in as ', this.userSrv.getUser());
        }
        if (req.status === 'completed')
        {
          const reqObj = JSON.stringify({req});
          console.log(reqObj);
          this.router.navigate(['completed-request-details', this.code.trim()], {queryParams: {obj:reqObj}});
        }
        else
        {   this.requestSrv.getById$(this.code.trim());// update the request subject
            await this.router.navigate(['/request-details/' + this.code.trim()])
        }
      }
      else
        this.showError=true;
    } catch (error) {
      console.warn('error', error);
    }
    
  }

  async goToNewRequest()
  {
    // if(this.auth.getUser) await this.userSrv.refreshUserSubject();
    const navigationExtras: NavigationExtras = {
      state: { isNew: true  }
    };
    
    this.router.navigate(['/request-form'], navigationExtras);
  }

  goto(page) {
    this.router.navigate(page);
  }

  async loginAsGuest() {// currently not using
    console.log('logging in as guest');
    const env = this;
    await this.auth.loginAsGuest()
    .then((usr) => {
      console.log(usr);
      const newuser = new UserModel();
      newuser.deviceToken = env.token;
      newuser.id = usr.uid;  // usr.uid;
      newuser.name = 'Guest_' + Date.now();
      newuser.createdDate = Date.now();
      newuser.guest = true;
      newuser.country=this.countryInfo.countryname;
      newuser.countrycode=this.countryInfo.countrycode;
      newuser.currencyCode=this.countryInfo.currencycode;
      newuser.currencySymbol=this.countryInfo.currencysymbol;
      return env.userSrv.add(newuser)
        .then(async (s) => {
          console.log(s);
          await this.userSrv.updateUserObj(newuser);
          return newuser;
        })
        .catch((e) => {
          console.warn('Error adding new user: ', e);
          return e; 
        });
    })
    .catch((e) => {
      console.warn(e);
    });
  }
}
