import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PayWithPaystackPage } from './pay-with-paystack.page';

const routes: Routes = [
  {
    path: '',
    component: PayWithPaystackPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PayWithPaystackPageRoutingModule {}
