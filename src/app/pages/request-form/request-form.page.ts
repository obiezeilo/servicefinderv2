import { Component, OnInit, Input } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import {FirebaseService } from '@services/firebase.service';
import {RequestService} from '@services/request.service';
import {ToolboxService } from '@services/toolbox.service';
import Request from '@models/request';
import { UserModel } from '@models/user';
import { PhotoService, UserPhoto } from '@services/photo.service';
import { ActionSheetController, ModalController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from '../../authentication/authentication.service';
import { UserService } from '@services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EmailService } from '@services/email.service';

@Component({
  selector: 'app-request-form',
  templateUrl: './request-form.page.html',
  styleUrls: ['./request-form.page.scss'],
})

export class RequestFormPage implements OnInit {
  requestForm: any;
  categories: any;
  isGuest = true;
  userObj: UserModel = new UserModel();
  states: any;
  disableSubmit = true;
  categoryQuestions = '';
  @Input() requestObj: Request;
  @Input() isNew = true;
  @Input() requestId: string;
  title = 'New Request';
  public showNewAccount = false;
  public newUser = new UserModel();// {name:'',password:'',confirmpassword:''}
  public password='';
  public confirmpassword='';
  public countries:any;
  public countryInfo:any;
  public mystate: string;

  validationMessages = {
    city: [
      { type: 'required', message: 'City is required.' }
    ],
    details: [
     { type: 'required', message: 'The details is required.' },
     { type: 'minlength', message: 'Name must be at least 2 characters long.' }
   ]
  };

  constructor(private formBuilder: FormBuilder, public tools: ToolboxService
    , private fbServ: FirebaseService, public userSrv: UserService,
      public actionSheetController: ActionSheetController, public auth: AuthenticationService,
      private reqService: RequestService, private modalCtrl: ModalController
    , public photoService: PhotoService, private emailSrv:EmailService,
    private route: ActivatedRoute, private router: Router)
  {
    this.route.queryParams.subscribe(params => {
      console.log(params);
      if (this.router.getCurrentNavigation() && this.router.getCurrentNavigation().extras
        && this.router.getCurrentNavigation().extras.state) {
          const obj = this.router.getCurrentNavigation().extras.state;
          this.isNew = (obj.isNew) ? true : false;
          if (!this.isNew) this.title="Edit Request";
          console.log('const req$', this.reqService.requestSubject$.value);
          this.requestObj = (this.reqService.requestSubject$.value) ? this.reqService.requestSubject$.value : new Request();
          if (this.requestObj.id == null && obj.reqId != null) this.requestObj.id = obj.reqId;
        }
    });
  };

  async getInfo()
  { console.log('getinfouser',this.auth.getUser);
    // Get the location information
    this.mystate = (await this.tools.getFromLocal('state')) ?? null;
    this.countryInfo=(await this.tools.getFromLocal('countryInfo')) ?? null;
    if (this.countryInfo) this.states = await this.tools.getStates(this.countryInfo.countrycode);

    if (this.isNew) this.requestObj = new Request();
    if (this.userSrv.getUser())
    {
      this.userObj = this.userSrv.getUser();//   await this.tools.getLoggedInUser() as UserModel;
      console.log('user is ', this.userObj);
      this.isGuest=this.userObj.guest;
    }
    else if (this.auth.getUser)
    {
      this.userObj.id = this.auth.getUser.uid;
      if (this.auth.getUser.refreshToken) this.userObj.deviceToken = this.auth.getUser.refreshToken;  // TODO refreshtoken Untested
      this.userObj.guest = true;
    }
    if (this.isGuest)  // guest
    { this.userObj.city = ''; 
      this.userObj.zip = '';
      if (this.countryInfo)
      {  this.userObj.country = this.countryInfo.countryname ?? '';
        this.userObj.countrycode = this.countryInfo.countrycode ?? '';
        this.userObj.currencyCode = this.countryInfo.currencycode ?? '';
        this.userObj.currencySymbol = this.countryInfo.currencysymbol ?? '';
      }
      this.userObj.state = this.mystate ?? '';
      this.userObj.name = 'Guest_' + Date.now();
      this.userObj.guest=true;
    }
    else
    { this.requestObj.city = this.userObj?.city;
      this.requestObj.zip = this.userObj?.zip;
      this.requestObj.latitude = this.userObj?.latitude;
      this.requestObj.longitude = this.userObj?.longitude;
      this.requestObj.email = this.userObj?.email;
    }
    this.requestObj.state = this.userObj?.state ?? this.mystate;
    console.log(this.userObj,this.requestObj); 

    if (!this.categories)
    {  
      try {
        this.categories = await (await fetch(environment.sfURL + 'categories.php', {
          method: 'GET',
          mode: 'cors'
        })).json();

        this.categories = this.categories.sort((a, b) => (a.name > b.name) ? 1 : -1);

        /* await this.fbServ.get('categories').then((c) => {
          this.categories = c.sort((a, b) => (a.name > b.name) ? 1 : -1);
          console.log('categories retrieved');
        }); */
      } catch (error) {
        this.tools.showAlert('Error','Error getting the categories');
        console.log(error);
      }
      
    }
    this.requestForm = this.formBuilder.group({
      city: [this.requestObj.city, Validators.compose([Validators.minLength(2), Validators.required]) ],
      state: [this.requestObj.state, Validators.required],
      zip: [this.requestObj.zip],
      category: [this.requestObj.category],
      details: [this.requestObj.details, Validators.compose([Validators.minLength(3), Validators.required]) ],
      email: [this.requestObj.email, Validators.required]
    });

    this.requestForm.valueChanges.subscribe(() => {
      this.disableSubmit = (this.requestForm.value.details == null 
        || this.requestForm.value.city == null || this.requestForm.value.state == null
        || this.requestForm.value.email == null);
    });
    

    if (this.requestObj.media)    { this.photoService.loadFromServer(this.requestObj.media)    }

    if (!this.userObj.country || this.userObj.country=='')
    {
      this.countries = await this.tools.getCountries();
    }
    else
    {
      // get the states of the country
      this.states = await this.tools.getStates(this.userObj.countrycode);
      this.countryInfo = await this.tools.getCountryInfo(this.userObj.countrycode);
    }
    
    if(this.userObj.countrycode) this.setCountry(this.userObj.countrycode);
  }

  ngOnInit() {
    console.log('reqObj', this.requestObj);
    console.log('req$', this.reqService.requestSubject$.value);
    this.getInfo();
    this.photoService.photoChanged.subscribe((s)=>{
      console.log('changed', s);
      this.disableSubmit=false;
    })
  };

  setCategory(evt) {
    this.requestObj.category = evt.target.value;
    this.categories.forEach(_cat => {
      if (_cat.name === evt.target.value)
      {
        this.categoryQuestions = _cat.questions.split('|');
      }
    });
  }

  /**
   * Get the states of the chosen country
   * @param country The chosen country
   */
  async setCountry(country:any)
  { console.log(this.countries);
    if (!this.countryInfo) this.countryInfo = this.countries.filter((c)=>c.countrycode == country)[0];
    console.log('selected country', this.countryInfo);
    this.states = await this.tools.getStates(this.countryInfo.countrycode);
  }

  showAccountForm()
  {
    this.showNewAccount = !this.showNewAccount;
  }

  async create() {
    
    /**
     * VALIDATION
     */
    if (this.requestForm.value.details == null || this.requestForm.value.category == null
      || this.requestForm.value.details === '' || this.requestForm.value.category === ''
      || this.requestForm.value.city === '' || this.requestForm.value.city == null || this.requestForm.value.email == null
      ) {
      this.tools.showAlert('Missing info', 'All fields are required!');
      return false;
    }

    // validate new user account creation
    if (this.showNewAccount)
    {
      if (this.newUser.name === '' && this.password === '')
      {
        this.tools.showAlert('Missing info', 'Name and password are required');
        return false;
      }
      else if (this.password !== this.confirmpassword)
      {
        this.tools.showAlert('Password mismatch', 'The passwords don\'t match');
        return false;
      }
    }
    // *********

    await this.tools.showLoading();

    // Create guest account if the user doesn't have an account
    if (!this.auth.getUser)
    {
      console.log('*** creating guest account or new account ***');
      let newAccount;
      try {
         newAccount = (this.showNewAccount) ? 
          await this.auth.registerUser({email:this.requestForm.value.email.trim(), password:this.password}) : 
          await this.auth.loginAsGuest();

          if (newAccount.code)
          {
            this.tools.hideLoading();
            let msg = newAccount.code;
            if (newAccount.code === 'auth/email-already-in-use')
              msg = 'The email address is already in use. If you have an account, please sign in first.';
            this.tools.showAlert('Error', msg);
            return false;
          }
      } catch (error) {
          console.warn(error);
          this.tools.hideLoading();
          this.tools.showAlert('Error', `There was an error: ${error}`);
          return false;
        
      }
      
      console.log({newAccount});
      // this.userObj.deviceToken = env.token;// TODO: GET THE TOKEN
      this.userObj.id = newAccount.uid;
      if (this.showNewAccount) this.userObj.name = this.newUser.name;
      this.userObj.createdDate = Date.now();
      this.userObj.guest = !this.showNewAccount;
      this.userObj.currencyCode = this.countryInfo.currencycode;
      this.userObj.country = this.countryInfo.countryname;
      this.userObj.countrycode = this.countryInfo.countrycode;
      this.userObj.currencySymbol = this.countryInfo.currencysymbol;
      this.userObj.email = this.requestForm.value.email.trim();;
      this.userObj.state = this.requestObj.state;
      this.userObj.zip = this.requestObj.zip;
      console.log('userObj', this.userObj);

      await this.userSrv.add(this.userObj)
        .then((s) => {
          console.log('new user added to firebase', s);
          this.userSrv.updateUserObj(this.userObj);
          })
          .catch((e) => {console.warn('Error adding new user: ', e);
          this.tools.hideLoading();
        });
    }
    
    console.log('requestform userobj',this.userObj);
    if (!this.userObj.id)
    {
      this.tools.showAlert('Error creating user','Could not create user for some reason')
      console.warn('no User ID');
      this.tools.hideLoading();
      return false;
    }
    // the ID
    let newRequestId:string;
    try {
      // let r = (Math.random() + 1).toString(36).substring(9);
      // Sometimes userobj.id is blank. This avoids that
      // if (!this.userObj.id) this.userObj.id = r + Date.now().toString().split('').reverse().join('')
      
      newRequestId = this.userObj.id.substring(0, 3).toUpperCase() + parseFloat(Date.now().toString().split('').reverse().join(''))
      console.log(newRequestId);
    } catch (error) {
      console.warn('error setting newrequestid:', error);
      this.tools.hideLoading();
      this.disableSubmit=false;
      return false;
    }

    // get the location
    if ((!this.isGuest
        && (this.userObj.city !==  this.requestObj.city
          || this.userObj.state !==  this.requestObj.state
          || this.userObj.zip !==  this.requestObj.zip)
          )
        || this.isGuest)
    {
      const addr = environment.geocoding.url + 'address=' +this.requestForm.value.city + ', ' + this.requestForm.value.state + ' ' + this.requestForm.value.zip
        + ', ' + this.userObj.country + '&key=' + environment.geocoding.api_key;

      const latlng = await this.tools.getLatLng(addr);
      if (latlng.status) {
        this.tools.showAlert('Location error',
          'Your location could not be determined. Please check the city and state and/or zip/postal code.<br>' + latlng.status);
        return false;
      }
      this.requestObj.latitude = latlng[0];
      this.requestObj.longitude = latlng[1];
      if (this.isGuest)
      {
        this.userObj.city = this.requestForm.value.city;
        this.userObj.state = this.requestForm.value.state;
        this.userObj.zip = this.requestForm.value.zip;
        this.userObj.latitude = latlng[0];
        this.userObj.longitude = latlng[1];
        this.userObj.country = this.countryInfo.countryname;
        this.userObj.countrycode = this.countryInfo.countrycode;
        this.userObj.currencySymbol=this.countryInfo.currencysymbol;
        this.userObj.currencyCode=this.countryInfo.currencycode;
      }
    }

    this.requestObj.category = this.requestForm.value.category;
    this.requestObj.details = this.requestForm.value.details;
    this.requestObj.userId = this.userObj.id;
    this.requestObj.userName = this.userObj.name;
    this.requestObj.status = 'new';
    this.requestObj.createdDate = Date.now();
    this.requestObj.statusDate = Date.now();
    this.requestObj.quoteCount = 0;
    this.requestObj.city = this.requestForm.value.city;
    this.requestObj.state = this.requestForm.value.state;
    this.requestObj.zip = this.requestForm.value.zip;
    this.requestObj.latitude = this.userObj.latitude;
    this.requestObj.longitude = this.userObj.longitude;
    this.requestObj.businessesNotified = 0;
    this.requestObj.email = this.requestForm.value.email.trim();

    // if (this.photoService.photos.length > 0) { this.requestObj.media = this.photoService.photos.map(_p=>_p.base64String); }
    const imageSrc = `request_photos/${newRequestId}/`;
    const promises = this.photoService.photos.map(async (p) => {
      console.log(p);
      return await this.fbServ.uploadFile(p.base64String, imageSrc + p.filepath.substr(p.filepath.lastIndexOf('/') + 1));
    });
    
    if (this.photoService.photos.length > 0) this.requestObj.media = await Promise.all(promises);

    this.reqService.add(this.requestObj, newRequestId)
      .then(async ()=>{

        // Send email to the user
    const emailtext = `Hello there,
Here's a copy of your Service Finder request

Request code: ${newRequestId}
Requested on ${new Date(Date.now()).toLocaleString()}
Category: ${this.requestObj.category}
Details: ${this.requestObj.details}
Location: ${this.requestObj.city}, ${this.requestObj.state} ${this.requestObj.zip}

Use your request code ${newRequestId} to check the status of your request. DO NOT LOSE IT.

Nearby businesses, if any, will be notified, and will be sending you quotes/estimates soon via the app.

Regards,

Service Finder Support`;

      const emailhtml = `<p>Hello there,</p>
        <p>Here's a copy of your Service Finder request</p>
        <p>----------------------------</p>
        <p>Requested on ${new Date(Date.now()).toLocaleString()}</p>
        <p><b>Request code:</b> ${newRequestId}
        <p><b>Category:</b> ${this.requestObj.category}</p>
        <p><b>Details:</b> ${this.requestObj.details}</p>
        <p><b>Location:</b> ${this.requestObj.city}, ${this.requestObj.state} ${this.requestObj.zip ?? ''}</p>
        <br>
        <p>The request code is <b>${newRequestId}</b>. Use it to locate your request. DO NOT LOSE IT.
        <p>Nearby businesses, if any, will be notified, and will be sending you quotes/estimates soon via the app.</p>
        <br>
        <p>Regards,</p>
        <br>
        <p>Service Finder Support</p>`;

      return this.emailSrv.sendEmail('info@servicefinderapp.com','Service Finder Support',this.requestObj.email.trim(),null
          ,'Your Service Finder request',emailtext,emailhtml)
    })
    .then(async ()=>{
      // update the guest's location
      console.log('updating the guest location')
      if (this.isGuest)
      { const obj = {city:this.requestObj.city, state:this.requestObj.state,
         zip:this.requestObj.zip, lat:this.requestObj.latitude, email:this.requestObj.email,
         lng:this.requestObj.longitude, currency:this.userObj.currencySymbol, currencycode:this.userObj.currencyCode}
        return await this.userSrv.update(this.auth.getUser.uid,obj)
      }
    })
/*     .then(async ()=>{
      console.log('creating account if necessary');
      return await this.createAccount()
    }) */
    .then(async ()=>{
      if (await this.tools.getFromLocal('countryInfo') == null)
      {
        await this.tools.saveToLocal('state', this.requestObj.state);
        return await this.tools.saveToLocal('countryInfo', this.countryInfo);
      }    
    })
    .then(async () => {
      await this.tools.hideLoading();
      await this.tools.showToast('Request created');
      this.tools.goto('/request-details/' + newRequestId, null,true);
      })
    .catch((e) => {
      this.tools.hideLoading();
      this.tools.showAlert('Error creating request', e);
      console.warn(e)});
  }

  // Save changes
  async save() {
    await this.tools.showLoading();
    if (this.requestForm.value.details == null || this.requestForm.value.category == null
      || this.requestForm.value.details === '' || this.requestForm.value.category === ''
      || this.requestForm.value.city === null || this.requestForm.value.state === null) {
      this.tools.showAlert('Missing info', 'All fields are required!');
      return false;
    }

    let latlng = null;
    // get the location
    if (this.userObj.city !==  this.requestForm.value.city
      || this.userObj.state !==  this.requestForm.value.state
      || this.userObj.zip !== this.requestForm.value.zip) {
      const addr = environment.geocoding.url + 'address=' +this.requestForm.value.city + ', '
        + this.requestForm.value.state + ' ' + this.requestForm.value.zip
        + ', ' + this.userObj.country + '&key=' + environment.geocoding.api_key;
      latlng = await this.tools.getLatLng(addr);
      if (latlng.status) {
        this.tools.showAlert('Location error',
          'Your location could not be determined. Please enter the correct city and state and/or zip/postal code.<br>' + latlng.status);
        return false;
      }
      this.requestObj.latitude = latlng[0];
      this.requestObj.longitude = latlng[1];
    }

    const imageSrc = `request_photos/${this.requestObj.id}/`;
    const promises = this.photoService.photos.map(async (p) => {
      if (p.filepath !== 'nofile')
      {
        console.log('Photo to upload', p);
        try {
        return await this.fbServ.uploadFile(p.base64String, imageSrc + p.filepath.substr(p.filepath.lastIndexOf('/') + 1));

        } catch (error) {
          console.warn('photo upload error', error);
        }
      }
      else
      { // If there are photos to delete, delete them
        try {
           if (this.photoService.filesToDelete)
            {  const indexOfFileToDelete = this.photoService.filesToDelete.indexOf(p.webviewPath);
              if (indexOfFileToDelete >=0)
              {
                try {
                await this.fbServ.deleteFile(p.webviewPath);  // delete from the server
                console.log(`Photo deleted ${p.webviewPath}`)

                } catch (error) {
                  console.warn('photo upload error', error);
                }
              }
            }
            return new Promise((resolve, reject) => {resolve(p.webviewPath);});
        } catch (e) {
          this.tools.hideLoading();
          this.tools.showAlert('Error with photos', e);
          console.warn(e);return false;
        }
      }
    });
    const pics = await Promise.all(promises);
    console.log({pics});
    const obj = {
      details: this.requestForm.value.details,
      category: this.requestForm.value.category,
      edited: !this.isNew,
      statusDate: Date.now(),
      media: pics,
      city: this.requestForm.value.city,
      state: this.requestForm.value.state,
      zip: this.requestForm.value.zip,
      ...(latlng != null && {latitude: latlng[0], longitude: latlng[1]})
    };
    // make the update
    console.log('req to update', obj);
    try {
      await this.reqService.update(this.requestObj.id, obj);
      const req=await this.reqService.getById(this.requestObj.id);
      this.reqService.requestSubject$.next(req);
    } catch (e) {
      this.tools.hideLoading();
      this.tools.showAlert('Error saving changes', e);
      console.warn('update error', e);
      console.warn('update error object',obj)
      return false;
    }

    // Create account if necessary
    try{
      if (this.showNewAccount) await this.createAccount();
    } catch (e) {
      this.tools.hideLoading();
      this.tools.showAlert('Error', e);
      console.warn('create account error', e);
      return false;
    }
    
    // Move on
    try{
      await this.tools.hideLoading();
      this.tools.showToast('Request edited');
      this.tools.goto('/request-details/' + this.requestObj.id);
    } catch (e) {
      this.tools.hideLoading();
      this.tools.showAlert('Error', e);
      console.warn('moving on error', e);
      return false;
    }

    /* this.reqService.update(this.requestId, obj)
      .then(async ()=>{ // create an account if needed
        return await this.createAccount();
      })
      .then(async () => {
        await this.tools.hideLoading();
        this.tools.showToast('Request edited');
        this.tools.goto('/request-details/' + this.requestId);
      })
      .catch((e) => {this.tools.hideLoading();this.tools.showAlert('Error saving changes', e); console.warn(e)});
 */  }


  public async showActionSheet(photo: UserPhoto, position: number) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Photos',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.photoService.deletePicture(photo, position);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          // Nothing to do, action sheet is automatically closed
         }
      }]
    });
    await actionSheet.present();
  }

  dismiss(sent= false) {
    this.modalCtrl.dismiss({
      dismissed: sent
    });
  }

  async createAccount()
  {
    // Create account if necessary
    if (this.showNewAccount)
    {
      const newUsr = await this.auth.convertGuestToAccount(this.requestObj.email.trim(), this.password);
      const token = await this.tools.getFromLocal('deviceToken');

      if (!newUsr.uid)
      {
        console.warn('error converting account', newUsr);
        this.tools.showAlert('Error creating account','There was an error creating an account');
        return false;
      }
      const obj = {id: newUsr.uid, name:this.newUser.name,
        city:this.requestObj.city,state:this.requestObj.state,
        zip:this.requestObj.zip, country: this.userObj.country,
        countrycode:this.userObj.countrycode, email:this.requestObj.email.trim(),
        phone:'', latitude:this.requestObj.latitude,
        longitude:this.requestObj.longitude,
        createdDate:this.requestObj.createdDate, deviceToken: token,
        currencySymbol:this.countryInfo.currencysymbol,guest:false}
      console.log('new user', obj);
      this.isGuest=false;
      return await this.userSrv.add(obj);
    }// if (new account)
    else return;
  }

  ngOnDestroy()
  {
    this.photoService.photos = [];
  }
}
