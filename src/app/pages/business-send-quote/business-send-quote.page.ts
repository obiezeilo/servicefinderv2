import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import {QuoteService} from '@services/quote.service';
import { Quote } from '@models/quote';
import {ToolboxService} from '@services/toolbox.service';
import { Business } from '@models/business';
import { BusinessService} from '@services/business.service';
import { MessageService } from '@services/message.service';
import { Message, Chat } from '@models/message';
import { EmailService } from '@services/email.service';

@Component({
  selector: 'app-business-send-quote',
  templateUrl: './business-send-quote.page.html',
  styleUrls: ['./business-send-quote.page.scss'],
})
export class BusinessSendQuotePage implements OnInit {
  details='';
  amount:number=null;
  request:any;
  note='';
  business:Business;
  quote:Quote;
  quoteplaceholder:string;
  currency: string;

  constructor(navParams: NavParams, public modalCtrl: ModalController,
    private emailSrv: EmailService, private quoteSrv: QuoteService,
    public tools: ToolboxService,
    private messageSrv: MessageService, private businessSrv: BusinessService)
  {
    this.request=navParams.get('request');
    this.quote=navParams.get('quote');
    console.log(navParams.data);

    this.business = this.businessSrv.getBusiness();
  }

  ngOnInit() {this.setPlaceHolder()}

  async setPlaceHolder()
  {
    this.currency =await this.tools.getCurrency()
    this.quoteplaceholder = this.currency + '0';
  }
  send()
  {
    this.tools.showLoading();
    const obj:Quote = new Quote();
    obj.amount=this.amount;
    obj.createdDate=Date.now();
    obj.requestId=this.request.id;
    obj.businessId=this.business.id;
    obj.businessName = this.business.name;
    obj.businessLocation=this.business.city + ', ' + this.business.state;
    obj.businessRating=this.business.rating;
    obj.businessRatingCount=this.business.ratingCount;
    obj.businessLogo=this.business.logo;
    obj.businessPhone=this.business.phone;
    obj.businessEmail=this.business.email;
    obj.status='new';
    obj.statusDate=Date.now();
    obj.note=this.note;

    // send a message
    const msg:Message = new Message();
    msg.businessId=this.business.id;
    msg.businessName=this.business.name;
    msg.createdDate=Date.now();
    msg.read=false;
    msg.requestId=this.request.id;
    msg.userId=this.request.userId;
    msg.userName=this.request.userName;
    if (this.business.logo) msg.businessLogo = this.business.logo;

    const chat:Chat =new Chat();
    chat.message=obj.note;
    chat.read=false;
    chat.sender='business';
    chat.createdDate=Date.now();
    msg.chats = [];
    msg.chats.push(chat);


    this.quoteSrv.add(obj)
      .then(async ()=>{// decrement the credit count
        const credits = this.business.credits - 1;
        await this.businessSrv.update(this.business.id, {credits})
        await this.businessSrv.refreshBusiness();
        return true;
      })
      .then(()=>{
        return this.messageSrv.add(msg);
      })
      .then(()=>{ // email the user

        if (!this.request.email) return;

        const emailtext = `You have received a quote for your request

Your Request code: ${obj.requestId}

Service Provider:

${obj.businessName}
${obj.businessLocation}
${obj.businessPhone}
${obj.businessEmail}

Quote: ${obj.amount}

Note from the provider: ${obj.note}

Reach out to the provider if you wish to move forward, or disregard if not.

Regards,

Service Finder Support`;

        const emailhtml = `<p>You have received a quote for your request</p>

          <p>----------------------------</p>
          <p><b>Your Request code:</b> ${obj.requestId}
          <p><b>Service Provider:</b></p>
          <b>${obj.businessName}</b>
          <br>${obj.businessLocation}
          <br>${obj.businessPhone}
          <br>${obj.businessEmail}
          <p><b>Quote:</b> ${this.currency}${obj.amount}</p>
          <p><b>Message from the provider:</b> ${obj.note}</p>
          <br>
          <p>Contact the provider if you wish to move forward, or disregard if not.</p>
          <br>
          <p>Regards,</p>
          <br>
          <p>Service Finder Support</p>`;

        return this.emailSrv.sendEmail('info@servicefinderapp.com','Service Finder Support',this.request.email,null
            ,'New Service Finder quote',emailtext,emailhtml)
      })
      .then(()=>{
        this.tools.hideLoading();
        this.tools.showToast('Quote has been sent');
        this.dismiss(true);
      })
      .catch((e)=>{
        console.log(e); 
        this.tools.showAlert('Error',e);
        this.tools.hideLoading();
      })
  }

  dismiss(sent=false) {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      dismissed: sent
    });
  }
}
