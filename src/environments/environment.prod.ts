export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyBWgsIpCFkpD2yumAY-YvVMoFIZ4xN0Quw",
    authDomain: "service-finder-live.firebaseapp.com",
    projectId: "service-finder-live",
    storageBucket: "service-finder-live.appspot.com",
    messagingSenderId: "154263432200",
    appId: "1:154263432200:web:ea38e7cf3b518219cc35bd",
    measurementId: "G-4EXN0L5DK6"
  },
  geocoding: {
    api_key:'AIzaSyAtCJbonqNrK-nE0UbInWBZjC7ZT2Hp8nQ',
    url: 'https://maps.googleapis.com/maps/api/geocode/json?'
  },
  stripe:
  {
    publishableKey:'pk_live_tEYsWeGPIlF4Vszilk2UmoA5',
    secretKey:'sk_live_gJ2K26rZJlYRjPvXf8ZC8MnZ',
    chargeURL:'https://servicefinderapp.com/api/stripefn.php?action=charge',
    newCustomerURL:'https://servicefinderapp.com/api/stripefn.php?action=createcustomer',
    newCardURL:'https://servicefinderapp.com/api/stripefn.php?action=addcreditcard',
    updateCardURL:'https://servicefinderapp.com/api/stripefn.php?action=editcreditcard',
    deleteCardURL:'https://servicefinderapp.com/api/stripefn.php?action=deletecreditcard'
  },
  paystack:
  {
    publishableKey:'pk_live_bae039d6a0af8a8adcae8436dec357aa9117bdea'
  },
  functionapi:{
    contactUrl : 'https://us-central1-service-finder-a7243.cloudfunctions.net/sendMail'
  },
  sfURL:'https://servicefinderapp.com/'
};
