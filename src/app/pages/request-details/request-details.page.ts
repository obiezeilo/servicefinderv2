import { Component, OnInit } from '@angular/core';
import Request from '@models/request';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import {ToolboxService } from '../../services/toolbox.service';
import {QuoteService} from '@services/quote.service';
import { Quote } from '@models/quote';
import { BusinessService } from '@services/business.service';
import { RequestService } from '@services/request.service';
import { Business } from '@models/business';
import { ModalController, AlertController } from '@ionic/angular';
import { RatingPage } from '../rating/rating.page';
import { PreviewPage } from '../preview/preview.page';
import { Subscription } from 'rxjs';
import { AuthenticationService } from '../../authentication/authentication.service';

@Component({
  selector: 'app-request-details',
  templateUrl: './request-details.page.html',
  styleUrls: ['./request-details.page.scss'],
})
export class RequestDetailsPage implements OnInit {
  reqId: string;
  req: Request = new Request();
  quotes: Quote[] = [];
  business: Business = new Business();
  quote: Quote;
  quoteSubscription: Subscription;
  public currency:string;

  constructor(private route: ActivatedRoute, public requestService: RequestService
    , public auth:AuthenticationService, private router: Router
    , public tools: ToolboxService, private quoteSrv: QuoteService, private alertCtrl: AlertController
    , private businessSrv: BusinessService, public modalController: ModalController) {
    }

  ngOnInit() {
    this.reqId = this.route.snapshot.paramMap.get('id');
    this.setCurrency()
    
// get the request
    this.requestService.getById$(this.reqId)
      .subscribe((r) => {
        this.req = r;
        // Get the quotes
        const arr = [];
        arr.push({property:'requestId', operator:'==', value:this.reqId});
        console.log('req$', this.requestService.requestSubject$.value);
        this.quoteSubscription = this.quoteSrv.get$(arr).subscribe((q: Quote[]) => {
          this.quotes = q;
        }); // quotes
      }); // request object
  }

  async setCurrency()
  {
    this.currency=await this.tools.getCurrency();
  }
  
  loadBusiness() {
    this.businessSrv.getById(this.req.businessId).then((b: any) => {
      this.business = b;
    });
  }

  async gotoEdit() {
    const navigationExtras: NavigationExtras = { state: {requestObj: this.req, isNew: false, requestId: this.reqId} };
    this.router.navigate(['request-form'], navigationExtras);
  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Delete this request',
      message: 'Are you sure you want to delete this request?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Delete',
          handler: (data) => {
            this.requestService.delete(this.reqId)
              .then(() => {
                this.tools.showToast('Request has been deleted');
                (this.auth.isGuest) ? this.tools.goto('start') : this.tools.goto('home');
              })
              .catch((e) => {
                console.warn('Error: ', e);
                this.tools.showAlert('Error', JSON.stringify(e));
              });
          }
        }
      ]
    });

    await alert.present();
  }

  async gotoDetails(quote) {
   this.tools.goto('quote-details', {request: this.req, quote});
  }

  async rate() {
    const modal = await this.modalController.create({
      component: RatingPage,
      componentProps: {request: this.req, quote: this.req, business: this.business}
    });
    modal.onDidDismiss().then((obj) => {
      if (obj.data.dismissed === true) {
        this.req.rating = obj.data.rating;
        this.req.ratingNotes = obj.data.ratingNotes;
        this.req.status = 'completed';
      } else {
        console.log('cancelled sending quote');
      }
    });
    return await modal.present();
  }

  sendMessage() {/* TODO get this working properly */
    this.tools.goto('messages', {requestId: this.req.id});
  }

  async openPreview(pic) {
    const modal = await this.modalController.create({
      component: PreviewPage,
      componentProps: {pic}
    });
    modal.onDidDismiss().then(() => {
      });
    return await modal.present();
  }

  ngOnDestroy() {
    if (this.quoteSubscription) { this.quoteSubscription.unsubscribe(); }
  }
}
