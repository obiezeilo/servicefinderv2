import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PayWithPaystackPageRoutingModule } from './pay-with-paystack-routing.module';

import { PayWithPaystackPage } from './pay-with-paystack.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PayWithPaystackPageRoutingModule
  ],
  declarations: [PayWithPaystackPage]
})
export class PayWithPaystackPageModule {}
