import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusinessRequestDetailsPageRoutingModule } from './business-request-details-routing.module';

import { BusinessRequestDetailsPage } from './business-request-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusinessRequestDetailsPageRoutingModule
  ],
  declarations: [BusinessRequestDetailsPage]
})
export class BusinessRequestDetailsPageModule {}
