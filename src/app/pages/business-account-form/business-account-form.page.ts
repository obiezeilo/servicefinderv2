import { Component, OnInit, ViewChild } from '@angular/core';
import {FirebaseService } from '@services/firebase.service';
import {ToolboxService } from '@services/toolbox.service';
import { Validators, FormBuilder,FormGroup} from '@angular/forms';
import {BusinessService} from '@services/business.service';
import {Business} from '@models/business';
import { AuthenticationService } from '@auth/authentication.service';
import { ModalController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { PhotoService, UserPhoto } from '@services/photo.service';
import { ActionSheetController } from '@ionic/angular';
import { UserService } from '@services/user.service';
import { count } from 'console';

@Component({
  selector: 'app-business-account-form',
  templateUrl: './business-account-form.page.html',
  styleUrls: ['./business-account-form.page.scss'],
})
export class BusinessAccountFormPage implements OnInit {
  @ViewChild('signupSlider',{read: null, static:true}) signupSlider;

	public businessForm: FormGroup;

  categories: any[] = [];
  isNew = true;
  businessAccount: Business;
  mycategories: string[]=[];
  userId:string;
  nopic = 'assets/img/no-picture.png';
  public newusercredits=0;
  public countries: Country[];
  countryInfo: Country;
  states: any;
  public mystate: string;
  disableSubmit = false;

  constructor(private formBuilder:FormBuilder, public tools:ToolboxService, public userSrv: UserService
    , private fbServ:FirebaseService, public modalController: ModalController
    , public actionSheetController: ActionSheetController, public photoService: PhotoService
    , private businessSrv: BusinessService,public authService: AuthenticationService )
    { }


  ngOnInit()
  {
    this.getInfo();
  }
  
  async getInfo() {
    this.categories = await this.tools.getJSONFromURL('categories.php'); 
    this.categories = this.categories.sort((a, b) => (a.name > b.name) ? 1 : -1);
    // The countries dropdown
    this.countries = await this.tools.getCountries();
    this.countryInfo=await this.tools.getFromLocal('countryInfo');
    console.log('ctryinfo',this.countryInfo);
    this.mystate = await this.tools.getFromLocal('state');
    if (this.countryInfo && this.countryInfo.countrycode) this.states = await this.tools.getStates(this.countryInfo.countrycode);
    console.log('countrycode',this.countryInfo)
    this.photoService.single = true;  // for the logo
    
    if (this.countryInfo && this.countryInfo.countrycode) this.newusercredits = (await this.tools.getJSONFromURL('creditcosts.php?c=' + this.countryInfo.countrycode)).newRegistrationCredits;
    
    if(this.authService.getUser){
      this.userId = this.authService.getUser.uid;
    }

    this.businessForm = this.formBuilder.group({
      userId:this.userId,
      category: [],
      name: ['', Validators.compose([Validators.required])],
      address: [''],
      city: ['', Validators.compose([Validators.required])],
      state: [this.mystate, Validators.compose([Validators.required])],
      country: [(this.countryInfo) ? this.countryInfo.countryname : '', Validators.compose([Validators.required])],
      zip: [''],
      phone: ['', Validators.compose([Validators.required])],
      email: [(this.userSrv.userSubject$.value) ? this.userSrv.userSubject$.value.email : '', Validators.compose([Validators.required])],
      website: [''],
      rating: [0],
      latitude:[0],
      longitude:[0],
      credits:[0],
      isActive:[true],
      password:['', Validators.compose([Validators.minLength(6)])],
      confirmPassword:['', Validators.compose([Validators.minLength(6)])]
    });
    this.businessForm.valueChanges.subscribe(() => {
      this.disableSubmit = (this.businessForm.value.name == null 
        || this.businessForm.value.city == null || this.businessForm.value.state == null
        || this.businessForm.value.email == null
        || this.businessForm.value.phone == null);
    });
    this.mycategories=[];
    this.businessAccount = this.businessSrv.getBusiness();
    
    if (this.businessAccount)
    {
      this.businessForm.controls['name'].setValue(this.businessAccount.name);
      this.businessForm.controls['address'].setValue(this.businessAccount.address);
      this.businessForm.controls['city'].setValue(this.businessAccount.city);
      this.businessForm.controls['state'].setValue(this.businessAccount.state);
      this.businessForm.controls['zip'].setValue(this.businessAccount.zip);
      this.businessForm.controls['country'].setValue(this.businessAccount.country);
      this.businessForm.controls['phone'].setValue(this.businessAccount.phone);
      this.businessForm.controls['email'].setValue(this.businessAccount.email);
      this.businessForm.controls['website'].setValue(this.businessAccount.website);
      this.businessForm.controls['longitude'].setValue(this.businessAccount.longitude);
      this.businessForm.controls['latitude'].setValue(this.businessAccount.latitude);
      this.businessForm.controls['isActive'].setValue(this.businessAccount.isActive);
      this.isNew=false;
      this.mycategories=this.businessAccount.category;
      this.setCountry(this.userSrv.userSubject$.value.countrycode);
      if (this.businessAccount.logo)
      {
        await this.photoService.loadFromServer([this.businessAccount.logo])
      }
    }
    else
    {
      this.businessAccount = new Business();
      if (this.userId)
      {
        this.businessAccount.id=this.userId;
        this.businessAccount.userId=this.userId;
      }
    }
  }


  setCategory(evt)
  {
    this.mycategories=evt.target.value;
  }
  
  async setCountry(country:any)
  { if (this.countries) this.countryInfo = this.countries.filter((c)=>c.countrycode == country)[0];
    await this.tools.saveToLocal('countryInfo', this.countryInfo);
    this.states = await this.tools.getStates(country);
    this.newusercredits = (await this.tools.getJSONFromURL('creditcosts.php?c=' + country)).newRegistrationCredits;
  }
  
  updateCategory(cat:string)
  {
    if (this.mycategories.indexOf(cat) === -1)
      this.mycategories.push(cat);
    else
      this.mycategories.splice(this.mycategories.indexOf(cat),1);
  }

  // Save changes
  async save()
  {
    // Some validation
    if (!this.authService.getUser || (this.authService.getUser && this.authService.isGuest))
    { // Passwords must match
      if (this.businessForm.controls['password'].value !== this.businessForm.controls['confirmPassword'].value)
      {
        this.tools.showAlert(`Passwords don't match`,'Please reconfirm your password');
        await this.tools.hideLoading();
        return false;
      }
      else if (this.businessForm.controls['password'].value.length < 6)
      {
        this.tools.showAlert(`Password is too short`,'The password must be at least 6 characters long');
        await this.tools.hideLoading();
        return false;
      }
    }
    await this.tools.showLoading()
    // confirm the address
    const addressUpdated = (this.businessAccount.city !== this.businessForm.value.city
      || this.businessAccount.state !== this.businessForm.value.state
      || this.businessAccount.zip !== this.businessForm.value.zip )

    const addr = environment.geocoding.url + 'address=' +this.businessForm.value.city + ', ' + this.businessForm.value.state 
      + ' ' + this.businessForm.value.zip + '&key=' + environment.geocoding.api_key;

    if (addressUpdated)
    { const latlng=await this.tools.getLatLng(addr);
      if (latlng == null)
      {
        this.tools.showAlert('Unknown address','Your location could not be determined. Please confirm the city, state and/or zip code');
        await this.tools.hideLoading();
        return false;
      }
      this.businessAccount.latitude=latlng[0];
      this.businessAccount.longitude=latlng[1];
    }
    console.log('formvalue',this.businessForm.value);
    this.businessAccount.name = this.businessForm.value.name;
    this.businessAccount.address = this.businessForm.value.address ?? '';
    this.businessAccount.city = this.businessForm.value.city;
    this.businessAccount.state = this.businessForm.value.state;
    this.businessAccount.zip = this.businessForm.value.zip;
    if (this.businessForm.value.country) this.businessAccount.country = this.businessForm.value.country;
    this.businessAccount.phone = this.businessForm.value.phone;
    this.businessAccount.email = this.businessForm.value.email;
    this.businessAccount.website = this.businessForm.value.website ?? '';
    this.businessAccount.isActive = this.businessForm.value.isActive;
    this.businessAccount.category=this.mycategories;
    if (this.isNew) this.businessAccount.credits=this.newusercredits;

    /** If creating an account from the start page */
    // Create the user object
    if (!this.authService.getUser || (this.authService.getUser && this.authService.isGuest))
    { 
      let newUsr: any;
      console.log('creating account with ' + this.businessAccount.email, this.businessForm.controls['password'].value)
      if (!this.authService.getUser)
      { const _obj = {email:this.businessAccount.email, password:this.businessForm.controls['password'].value}
        newUsr = await this.authService.registerUser(_obj);
      }
      else if (this.authService.isGuest)
      {
        newUsr = await this.authService.convertGuestToAccount(this.businessAccount.email, this.businessForm.controls['password'].value);
      }

      if (!newUsr.uid)
      {
        console.warn('error converting account', newUsr.code);
        let errorMessage = 'There was an error creating an account';
        if (newUsr.name || newUsr.code) errorMessage='Error: ' + newUsr.code.replace('auth/','');
        this.tools.showAlert(`Error creating account`, errorMessage);
        await this.tools.hideLoading();
        return false;
      }
      
      // Set the business' ID
      this.businessAccount.id = newUsr.uid;
      this.businessAccount.userId = newUsr.uid;

      const token = await this.tools.getFromLocal('deviceToken');
      this.businessAccount.deviceToken = token ?? '';

      const obj = {id: newUsr.uid, name:this.businessAccount.name,
        city:this.businessAccount.city,state:this.businessAccount.state,
        zip:this.businessAccount.zip, email:this.businessAccount.email,
        phone:'', latitude:this.businessAccount.latitude,
        longitude:this.businessAccount.longitude,
        country:this.countryInfo.countryname,
        countrycode:this.countryInfo.countrycode,
        createdDate:Date.now(),currencySymbol:this.countryInfo.currencysymbol,
        currencyCode:this.countryInfo.currencycode,
        deviceToken:token ?? '', guest:false}
      console.log('new user', obj);
      await this.userSrv.add(obj);
      this.userSrv.userSubject$.next(obj);
    }
    

    // Upload to Firebase storage
    const imageSrc = `logos/${this.businessAccount.id}/`;
    if (this.photoService.photos[0])
    { const _photo = this.photoService.photos[0];
      // tslint:disable-next-line: max-line-length
      if (_photo.filepath !== 'nofile')
        this.businessAccount.logo = await this.fbServ.uploadFile(_photo.base64String, imageSrc + _photo.filepath.substr(_photo.filepath.lastIndexOf('/') + 1));
      else
        this.businessAccount.logo = _photo.webviewPath;
    }
    // delete from the server if necessary
    if (this.photoService.filesToDelete.length > 0)
    { this.businessAccount.logo = null;
      this.photoService.filesToDelete.forEach(async (p)=>{
        await this.fbServ.deleteFile(p);
      })
    }
    let prmUpdate: Promise<any>, msg='';
    const env=this;
    if (this.isNew)
    {
      msg='Business account created'

      prmUpdate = new Promise((resolve, reject)=>{
        env.businessSrv.add(env.businessAccount)
          .then(()=>{resolve (true);})
          .catch((e)=>reject(e));
        });
    }
    else
    {
      msg='Business account updated';
      prmUpdate = new Promise((resolve, reject)=>{
        env.businessSrv.update(env.businessAccount.id, env.businessAccount)
          .then(()=>{resolve (true);})
          .catch((e)=>reject(e));
        });
    }

    console.log('obj', this.businessAccount)
    prmUpdate.then(async ()=>{
      try {
        this.businessSrv.updateBusinessObject(Object.assign({}, this.businessAccount));
        await this.tools.showToast(msg);
        await this.tools.hideLoading();
        this.tools.goto('/business-account',null,true);
      }
      catch (error) {
          this.tools.hideLoading();
          this.tools.showAlert('Error', error);
      }
    }).catch((e) => {
      this.tools.hideLoading();
      this.tools.showAlert('Error', e);
      console.warn(e)});
  }

  ngOnDestroy()
  {
    this.photoService.photos = [];
    this.businessForm = null;
  }
}

export interface Country
{
  countrycode: string;
  countryname: string;
  currencycode: string;
  currencyname: string;
  currencysymbol: string;
}