import { Component, OnInit } from '@angular/core';
import {RequestService} from '@services/request.service';
import {QuoteService} from '@services/quote.service';
import Request from '@models/request';
import { Quote } from '@models/quote';
import {ToolboxService} from '@services/toolbox.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { BusinessSendQuotePage } from '../business-send-quote/business-send-quote.page';
import { Business } from '@models/business';
import { Subscription } from 'rxjs';
import { PreviewPage } from '../preview/preview.page';
import { BusinessService } from '@services/business.service';


@Component({
  selector: 'app-business-request-details',
  templateUrl: './business-request-details.page.html',
  styleUrls: ['./business-request-details.page.scss'],
})
export class BusinessRequestDetailsPage implements OnInit {

  request: Request;
  businessAccount: Business;
  quote: Quote = null;
  quoteSubscription: Subscription;
  paramSubscription: Subscription;
  currency: any;

  constructor(public tools: ToolboxService, private route: ActivatedRoute, private router: Router,
              private requestSrv: RequestService, private quoteSrv: QuoteService,
              public modalController: ModalController, public businessSrv: BusinessService) 
  {
  }

  ngOnInit() 
  {
    this.paramSubscription = this.route.queryParams
    .subscribe(params => {
      if (params && this.router.getCurrentNavigation().extras.state != undefined) { // console.log(params);
        this.request = this.router.getCurrentNavigation().extras.state.params as Request;
          this.businessAccount = this.businessSrv.getBusiness();
          const arr = [];
          arr.push('requestId|==|' + this.request.id);
          arr.push('businessId|==|' + this.businessAccount.id);
          this.quoteSubscription = this.quoteSrv.get(arr)
            .then((q) => {
              this.quote = q[0] as Quote;
              console.log(this.quote);
            });
      }
    });
    this.setCurrency()
   }

   async setCurrency()
   {
     this.currency=await this.tools.getCurrency();
   }

  async sendQuote() {
    const modal = await this.modalController.create({
      component: BusinessSendQuotePage,
      componentProps: {request: this.request, quote: this.quote}
    });
    modal.onDidDismiss().then((obj) => {
      if (obj.data.dismissed === true) {console.log(obj);
       let arr = [];
       arr.push('requestId|==|' + this.request.id);
       arr.push('businessId|==|' + this.businessAccount.id);
       this.quoteSrv.get(arr)
          .then((q) => {
            console.log('refreshing quote...', q);
            this.quote = q[0] as Quote;
          });
      } else {
      console.log('cancelled sending quote');
      }
    });
    return await modal.present();
  }

  backToRequests() {
    this.tools.goto('business-account');
  }


  async openPreview(pic) {
    const modal = await this.modalController.create({
      component: PreviewPage,
      componentProps: {pic}
    });
    modal.onDidDismiss().then(() => {
      });
    return await modal.present();
  }

  sendMessage() {
    this.tools.goto('messages', {requestId: this.request.id, businessId: this.quote.businessId});
  }

/*   ngOnDestroy() {
    if (this.quoteSubscription) this.quoteSubscription.unsubscribe();
    if (this.paramSubscription) this.paramSubscription.unsubscribe();
  } */
}
