import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticateService } from '../../services/auth.service';
import { BusinessService } from '@services/business.service';

@Component({
  selector: 'app-popup-menu',
  templateUrl: './popup-menu.component.html',
  styleUrls: ['./popup-menu.component.scss'],
})
export class PopupMenuComponent implements OnInit {
  constructor(private authService: AuthenticateService,
    private router: Router,
    private popoverController: PopoverController, 
    public businessService: BusinessService,
    private route: ActivatedRoute) { }

  currentPage = this.router.url;

  ngOnInit() { console.log(this.router.url)}

  comingsoon()
  {    alert('Coming soon');  }

  goTo(page)
  { this.dismissMenu().then(()=>{
      this.router.navigate([page], {relativeTo: this.route}).then(()=>{
      console.log('Going to ' + page);
      }).catch((e)=>{console.warn('Error navigating: ',e)})
    });
  }

  goHome()
  {
    (this.businessService.businessSubject$.value) ? this.goTo('/business-account') : this.goTo('/tabs/home');
  }

  async dismissMenu() {
   await this.popoverController.dismiss();
  }

  logOut(){
    this.authService.logoutUser()
    .then(() => {
      this.goTo('start');
    })
    .catch(error => {
      console.log(error);
    })
  }
}
