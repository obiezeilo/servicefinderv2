export class Message {
    id?: string;
    userId: string;
    userName: string;
    requestId:string;
    businessId: string;
    businessName: string;
    businessLogo:string;
    chats:Chat[];
    createdDate: number;
    read: boolean;
    lastChatAddedTime:number;
  }

  export class Chat{
    message:string;
    read:boolean=false;
    sender:string;
    createdDate:number;
  }