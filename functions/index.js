const functions = require('firebase-functions');
// const stripe = require('stripe')('sk_test_1IsCDI8YgfaSM5omaccRVNGt');
const admin = require('firebase-admin');
const GeoFire = require("geofire");
const nodemailer = require('nodemailer');
const cors = require('cors')({origin: true});

// const adminConfig = JSON.parse(process.env.FIREBASE_CONFIG);
admin.initializeApp();

const default_search_radius = 145;  // TODO change to 40

const db = admin.firestore();
//admin.initializeApp(functions.config().firebase);


/******** Notify user on new quote ***************/
exports.notifyQuote = functions.firestore.document('quotes/{quoteId}')
    .onCreate(async (snap, context) =>
{
    // This should rarely occur, but it's just a precaution
    if (!snap.data()) {console.log('No quote object');  return false;}

    const quote = snap.data();

    // Increment the quote count in the requests object
    const increment = admin.firestore.FieldValue.increment(1);

    const requestRef = db.collection("requests").doc(quote.requestId);

    await requestRef.update({quoteCount: increment});

    // Get the request object
    const req = await requestRef.get();
    
    if (!req.exists)
    {
        console.log('unable to read a nonexistent request from quote: ', quote);
        return false;
    } 

    // Get the user's device token
    let _devicetoken;
    if (!req.data().deviceToken)
    {
        const _user=(await db.collection('users').doc(req.data().userId).get()).data();

        _devicetoken= _user.deviceToken;
        if (!_devicetoken)
        {
            console.log('User ' + req.data().userId + ' does not have a device token ');
            return false;
        } 
        await requestRef.update({deviceToken: _devicetoken});
    }
    else
        _devicetoken = req.data().deviceToken;

    let title = 'New Service Finder quote';
    let message = 'You have a new quote from ' + quote.businessName;
    let page = 'request-details/'+quote.requestId;

    // Notify the user of the new quote
    return sendPushNotification(_devicetoken,title,message, 
        {   page:page, 
            title:title,
            body:message
        });
})//onWrite

// Increment the business ratings and notify the business
exports.updateBusinessRatingsCount = functions.firestore.document('requests/{id}')
.onUpdate((change, context) => {

   if (change.before.data().rating === change.after.data().rating) return null;
   var req=change.after.data();
   const businessId = req.businessId;

   const incrementRating = admin.firestore.FieldValue.increment(parseInt(change.after.data().rating));
   const incrementRatingCount = admin.firestore.FieldValue.increment(1);

    return db.collection("businesses").doc(businessId)
        .update({ratingCount: incrementRatingCount, rating: incrementRating})
        .then(async ()=>{
            return await db.collection("businesses").doc(req.businessId).get();
        })
        .then((b)=>{
            const dataParam = 
            {   page:'BusinessAccountPage', 
                requestId:context.params.id, 
                title:'Service rated', 
                body:'Your service was rated ' + req.rating + ' out of 5'
            }
        
            return sendPushNotification(b.data().deviceToken
                ,'Service rated', 'Your service was rated ' + req.rating + ' out of 5',dataParam );
        });               
})   // exports.updateBusinessRatingsCount


// Find nearby businesses and notify them
exports.notifyBusinessesOnNewRequest = functions.firestore.document('requests/{id}')
    .onCreate(async (snap, context) => {
    const req=snap.data();
    
    // Find businesses in the same state and category
    const b = await db.collection("businesses")
            .where("state", "==", req.state)
            .where("category", "array-contains", req.category)
            .where("isActive", "==", true).get();

    var bizTokens = [];
    b.forEach((s) => {
        console.log('biz token',s.data().deviceToken);
        if (s.data().deviceToken !== null) {
            var biz = s.data();

            var distance = getDistance(biz.latitude, biz.longitude, req.latitude, req.longitude, true);
            console.log({distance});
            if (distance <= default_search_radius)  bizTokens.push(biz.deviceToken);
        }
        else {
            console.log("No token for ", biz.name);
        }
    });

    if (bizTokens.length === 0) {
        console.log("no biz notified");
        return false;
    }
    var dataParam = {
        page: 'BusinessAccountPage',
        user_id: req.userId,
        category: req.category,
        title: 'New Request',
        body: 'You have a new ' + req.category + ' request from ' + req.city
    };
    await db.collection("requests").doc(context.params.id).update({ businessesNotified: bizTokens.length });
    return sendPushNotification(bizTokens, 'New request', req.category + ' request from ' + req.city, dataParam);
})

exports.notifyNewChat = functions.firestore.document('messages/{messageId}')
    .onWrite(async (change, context) => {

   const message=change.after.data();
   
    const chat=message.chats[message.chats.length-1];
    if (!message) return false;

    var tokenToNotify, sender;
    if (chat.sender === "user")
    {   tokenToNotify = await db.collection("businesses").doc(message.businessId).get().then(b=>{return b.data().deviceToken});
        sender = message.userName;
    }
    else
    {
        tokenToNotify = await db.collection("users").doc(message.userId).get().then(b=>{return b.data().deviceToken});
        sender = message.businessName;
    }

    var dataParam = 
    {   page:'Messages', 
        title:'New Message', 
        body:'You have a new message from ' + sender
    }
    
    sendPushNotification(tokenToNotify,'New message', 'You have a new message from ' + sender, dataParam );
    return true;
})

function sendPushNotification(recipients, title, body, objParams)
{
    var payload = {
        notification:{
            title: title,
            body: body,
            sound: 'default',
            badge: '1',
            click_action:"FCM_PLUGIN_ACTIVITY"
        },
        data: objParams
    }
   
    return admin.messaging().sendToDevice(recipients, payload)
        .then((response)=>
        {
            console.log("Notification success: ", JSON.stringify(response));
            return {success:true};
        })
        .catch((error)=>{ 
            console.log("Error sending notification: " + JSON.stringify(error)); 
            console.log("Payload: " + JSON.stringify(payload));
        });//sendtodevice
}

//https://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula
function getDistance(lat1, lng1, lat2, lng2, miles)
{
    if (typeof miles === "undefined"){miles=false;}
    function deg2rad(deg){return deg * (Math.PI/180);}
    function square(x){return Math.pow(x, 2);}
    var r=6371; // radius of the earth in km
    lat1=deg2rad(lat1);
    lat2=deg2rad(lat2);
    var lat_dif=lat2-lat1;
    var lng_dif=deg2rad(lng2-lng1);
    var a=square(Math.sin(lat_dif/2))+Math.cos(lat1)*Math.cos(lat2)*square(Math.sin(lng_dif/2));
    var d=2*r*Math.asin(Math.sqrt(a));
    if (miles){return d * 0.621371;} //return miles
    else{return d;} //return km
}


exports.getRequestsData = functions.https.onRequest((req,res)=>{
    var days = 7; // Days you want to subtract
    var date = new Date();
    var last = new Date(date.getTime() - (days * 24 * 60 * 60 * 1000));
    var ftime = last.getTime();
    var tdays = 9;
    var tlast = new Date(date.getTime() - (tdays * 24 * 60 * 60 * 1000));
    var ttime = tlast.getTime();
        db.collection("requests").where("status", "==", 'completed')
        .get()
        .then((querySnapshot) =>{
            querySnapshot.forEach((doc) => {
                // doc.data() is never undefined for query doc snapshots
               // console.log(doc.id, " => ", doc.data());
               let data = doc.data();
               if(data.status_date >  ttime &&  data.status_date < ftime){
               let message = 'Hello '+data.fullname+ ' please rate for our bussiness';
               let quote = data.quote;
               let title = 'Please rate for '+quote.businessprofile_name;
               let page ='RatingPage';
               let tab = 'new';
               let userid = data.user_deviceToken;// 'Xj0lk3XON6YC84fKvzjrsX1A1202'; //data.userid; //
              //  let userid = "KbHEdCaRPqOOaVWMpXWCJn0JV4c2";
               // let userid = "dk236VHFQp-aARLnsP1mGL:APA91bFNZvquS5kId2gTduFF_kQ-LwpEAd60lfgD_vFcyZjdaupNrpvjV3KtbL3ipQjND8pWL6jRhyUl6H0pnCgLO7tbMOsFC_hqn0EG-EaXzoM35HDSmRuWnv_24X3Wezi1YeFqxY29";
              // sendNotification(userid,title,message, {page:page, tab:tab, bizprofile_id:quote.businessprofile_id, title:title, body:message}); 
               var payload = {
                    notification:{
                        title: title,
                        body: message,
                        sound: 'default',
                        badge: '1',
                        click_action:"FCM_PLUGIN_ACTIVITY"
                    },
                    data: {page:page, tab:tab, bizprofile_id:quote.businessprofile_id, title:title, body:message}
                }
                   admin.messaging().sendToDevice(userid, payload)
                            .then((response)=>
                            {
                                console.log("Notification success: ", JSON.stringify(response));
                                return true;
                            })
                            .catch((error)=>{ console.log("Error sending notification: " + JSON.stringify(error)); console.log("Payload: " + JSON.stringify(payload));
                            });
               
               res.send('<tr><td>'+title+'</td><td>'+message+'</td><td>'+userid+'</td></tr>');
              }
            });
            return true;
        })
        .catch((error) => {
            console.log("Error getting documents: ", error);
            return false;
        });
        //return false;
    
    });
/*
exports.scheduledFunctionCrontab = functions.pubsub.schedule('30 11 * * *')
  .timeZone('America/New_York') // Users can choose timezone - default is America/Los_Angeles
  .onRun((context) => {
    var days = 7; // Days you want to subtract
    var date = new Date();
    var last = new Date(date.getTime() - (days * 24 * 60 * 60 * 1000));
    var ftime = last.getTime();
    var tdays = 9;
    var tlast = new Date(date.getTime() - (tdays * 24 * 60 * 60 * 1000));
    var ttime = tlast.getTime();
        db.collection("requests").where("status", "==", 'completed')
        .get()
        .then((querySnapshot)=> {
            querySnapshot.forEach((doc)=> {
                // doc.data() is never undefined for query doc snapshots
               // console.log(doc.id, " => ", doc.data());
               let data = doc.data();
               if(data.status_date >  ttime &&  data.statusDate < ftime){
               let message = 'Hello '+data.userName+ ' please rate for our bussiness';
               let quote = data.businessQuote;
               let title = 'Please rate for '+quote.businessName;
               let page ='RatingPage';
               let tab = 'new';
               let userid = data.user_deviceToken;// 'Xj0lk3XON6YC84fKvzjrsX1A1202'; //data.userid; //
              //  let userid = "KbHEdCaRPqOOaVWMpXWCJn0JV4c2";
               // let userid = "dk236VHFQp-aARLnsP1mGL:APA91bFNZvquS5kId2gTduFF_kQ-LwpEAd60lfgD_vFcyZjdaupNrpvjV3KtbL3ipQjND8pWL6jRhyUl6H0pnCgLO7tbMOsFC_hqn0EG-EaXzoM35HDSmRuWnv_24X3Wezi1YeFqxY29";
              // sendNotification(userid,title,message, {page:page, tab:tab, bizprofile_id:quote.businessprofile_id, title:title, body:message}); 
               var payload = {
                    notification:{
                        title: title,
                        body: message,
                        sound: 'default',
                        badge: '1',
                        click_action:"FCM_PLUGIN_ACTIVITY"
                    },
                    data: {page:page, tab:tab, bizprofile_id:quote.businessprofile_id, title:title, body:message}
                }
                   admin.messaging().sendToDevice(userid, payload)
                        .then((response)=>
                        {
                            console.log("Notification success: ", JSON.stringify(response));
                            return true;
                        })
                        .catch((error)=>{ console.log("Error sending notification: " + JSON.stringify(error)); console.log("Payload: " + JSON.stringify(payload));
                        });
               
               res.send('<tr><td>'+title+'</td><td>'+message+'</td><td>'+userid+'</td></tr>');
              }
            });
            return true;
        })
        .catch((error) => {
            console.log("Error getting documents: ", error);
            return false;
        });
        
})*/