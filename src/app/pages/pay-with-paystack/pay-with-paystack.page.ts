import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
declare var PaystackPop: any;

@Component({
  selector: 'app-pay-with-paystack',
  templateUrl: './pay-with-paystack.page.html',
  styleUrls: ['./pay-with-paystack.page.scss'],
})
export class PayWithPaystackPage implements OnInit {
  businessEmail: string;
  totalcost: number;

  constructor(public modalCtrl: ModalController) { }

  ngOnInit() {
    console.log(this.businessEmail, this.totalcost);
    let env=this;
    PaystackPop.setup({
      key: environment.paystack.publishableKey,
      email: this.businessEmail,
      amount: this.totalcost * 100,
      container: 'paystackEmbedContainer',
      callback: function(response){
           console.log('successfully subscribed. transaction ref is ', response);
           env.dismiss(response);
       },
     });
     
  }

  dismiss(data=null)
  {
    this.modalCtrl.dismiss({
      'dismissed': false,
      'info' : data
    });
  }

}
