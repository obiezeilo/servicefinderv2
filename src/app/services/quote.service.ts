import { Injectable } from '@angular/core';
import { FirebaseService, FirestoreQuery} from './firebase.service'
import { Quote } from '@models/quote'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuoteService {

  constructor(private fbServ: FirebaseService) { }

  async add(obj:Quote)
  {
    return await this.fbServ.add('quotes',obj, obj.id);
  }

  async update(id:string,obj)
  {
    return await this.fbServ.update('quotes',id,obj);
  }

  get(filter:Array<string>, arr?: Array<string>):any
  {
    return this.fbServ.get('quotes', filter, arr, 'createdDate');
  }

  get$(filter: FirestoreQuery[], arr?: Array<string>, orderby?: Array<string>): Observable<Quote[]>
  { const orderArray = (orderby) ? [...orderby, 'createdDate'] : ['createdDate'];
    return this.fbServ.get$('quotes', filter, arr, orderArray);
  }

  async getById(id:string)
  {
    const q = await this.fbServ.getById('quotes', id);
    return q as Quote;
  }

  delete(id)
  {
    return this.fbServ.delete('quotes',id);
  }
}
