import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { RequestService } from '@services/request.service';
import { QuoteService } from '@services/quote.service';
import { ToolboxService } from '@services/toolbox.service';
import { BusinessSendQuotePage } from '@pages/business-send-quote/business-send-quote.page';
import { EmailService } from '@services/email.service';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.page.html',
  styleUrls: ['./rating.page.scss'],
})
export class RatingPage implements OnInit {

  objRating: any = {
    rating: 0,
    ratingNotes: null,
    status: 'completed',
    statusDate: 0
  };
  comments: string;
  request: any;
  business: any;
  allObj: any;
  ratingsDesc: string;
  quote: any;
  @Input() requestId: string;

  constructor( private requestSrv: RequestService, private modalCtrl: ModalController
    , private tools: ToolboxService, private quoteSrv: QuoteService,
    private emailSrv: EmailService) { }

  ngOnInit() {
    console.log('business=', this.business);
    console.log('quote=', this.quote);

    this.objRating = {
      rating: 0,
      ratingNotes: null,
      status: 'completed',
      statusDate: 0,
      businessId: this.business.id,
      businessQuote:this.quote
    };
  }

  dismiss(sent= false) {
    this.modalCtrl.dismiss({
      dismissed: sent,
      rating: this.objRating.rating,
      ratingNotes: this.objRating.ratingNotes
    });
  }

  // Set the rating
  setRating(n?) {
    this.objRating.rating = n + 1;
    this.ratingsDesc = this.objRating.rating + ' stars';
  }

  // Save the ratings & return to the other page
  saveRatings() {

    this.tools.showLoading();
    this.quote.businessRatingCount++;
    this.quote.businessRating = this.quote.businessRating + this.objRating.rating;
    this.objRating.businessQuote = this.quote;
    this.objRating.statusDate = Date.now();
    
    console.log('Quote', this.quote);
    console.log('this.objRating',this.objRating)

    this.requestSrv.update(this.requestId, this.objRating)
      .then((r) => {
        return this.quoteSrv.update(this.quote.id, this.objRating);
      })
      .then(()=>{ // email the user
// TODO Move all emails to PHP
        if (!this.request.email) return;

        const emailtext = `You were rated ${this.objRating.rating}/5 stars for your service.

Regards,

Service Finder Support`;

        const emailhtml = `<p>You were rated ${this.objRating.rating}/5 stars for your service.</p>
          <br>
          <p>Regards,</p>
          <br>
          <p>Service Finder Support</p>`;

        return this.emailSrv.sendEmail('info@servicefinderapp.com','Service Finder',this.quote.businessEmail,null
            ,'New Service Finder rating',emailtext,emailhtml)
      })
      .then(() => {
        return this.tools.showToast('Thank you for rating this service');
      })
      .then(() => {
        this.dismiss(true); })
      .catch((e) => {
        console.warn('Error rating: ', e);
        this.tools.hideLoading();
    }).finally(async ()=>{
      await this.tools.hideLoading();
    });
  }
}
