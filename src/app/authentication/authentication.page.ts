import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { ToolboxService } from '@services/toolbox.service';
import { BusinessService } from '@services/business.service';
import { UserService } from '@services/user.service';
import { StorageService } from '@services/storage.service';
import { Capacitor } from '@capacitor/core';
import { UserModel } from '@models/user';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.page.html',
  styleUrls: ['./authentication.page.scss'],
})
export class AuthenticationPage implements OnInit {
  url = ''; // The URL we're at: login, signup, or reset.
  pageTitle = 'Sign In';
  actionButtonText = 'Sign In';
  token='';
  public disableSubmit=false;

  constructor(
    private readonly router: Router,
    private readonly auth: AuthenticationService,
    private tools: ToolboxService,
    private businessSrv: BusinessService,
    private userSrv: UserService, private storageSrv: StorageService )     {    }
    public countryInfo:any;
  ngOnInit()
  {
    // First we get the URL, and with that URL we send the
    // proper information to the authentication form component.
    this.url = (this.router.url.substr(1) != '') ? this.router.url.substr(1) : 'login';

    if (this.url === 'signup')
    {
      this.pageTitle = 'Create your account';
      this.actionButtonText = 'Create Account';
    }
    else if (this.url === 'reset') {
      this.pageTitle = 'Reset your password';
      this.actionButtonText = 'Reset Password';
      }
    else if (this.url === 'login')
    { // if already logged in, go to home
      console.log('logged in')
      if (this.auth.getUser !== null) this.router.navigateByUrl('/tabs/home');
    }
  }

  async login(_email: string, _password: string) {
    this.disableSubmit=true;
    this.tools.showLoading();
    try {
      // Set the device token
      if (Capacitor.isNativePlatform()) {
        this.token = await this.storageSrv.get('deviceToken');
        if (this.token == null)
        {
          console.log('token not found. Setting to null');
          this.token='';
          // return false;
        }
      }
      const theUser = await this.auth.signIn({email: _email, password:_password});
      if (theUser === undefined || theUser == null)
      {
        this.tools.showAlert('Wrong email/password','Either the email address or password is incorrect. Try again');
        this.tools.hideLoading();
        return false;
      }

      // Update the devicetoken in FB
      console.log('Updating the devicetoken',this.token);
      await this.userSrv.update(theUser.uid,{deviceToken:this.token});
      
      // Refresh the user subject
      console.log('Refresh the user subject',this.token);

      await this.userSrv.refreshUserSubject();

      // set countryInfo in storage
      const obj={countryname:this.userSrv.userSubject$.value.country,
        countrycode:this.userSrv.userSubject$.value.countrycode,
        currencycode:this.userSrv.userSubject$.value.currencyCode,
        currencysymbol:this.userSrv.userSubject$.value.currencySymbol,
        currencyname:this.userSrv.userSubject$.value.currencyCode}
      await this.tools.saveToLocal('countryInfo', obj);

      // Get business data, if any
      console.log('getting business from ', theUser.uid)

      const biz = await this.businessSrv.getById(theUser.uid);

      if (biz)
      { console.log('business found', biz);
        // update the deviceToken
        await this.businessSrv.update(biz.id,{deviceToken:this.token})
        await this.businessSrv.refreshBusiness();
      }
      else
      {console.log('No business account', biz)}

      console.log('biz saved in subj', this.businessSrv.getBusiness());
      this.tools.hideLoading();
      (biz) ? this.router.navigateByUrl('/business-account') : this.router.navigateByUrl('/tabs/home');
    } catch (error) {
      // console.log(`Either we couldn't find your user or there was a problem with the password`, error );
      console.warn(error);
      this.tools.hideLoading();
      let msg = error.message;
      if (error.code === 'auth/user-not-found') msg = 'The email address or password is incorrect';
      this.tools.showAlert('Login error', msg);
      this.disableSubmit=false;
    }
  }

  async signUp(_email: string, _password: string) {
    
    this.disableSubmit=true;
    this.tools.showLoading();
    try {
      const create = await this.auth.registerUser({email: _email, password:_password});
      console.log(create);
      if (create.name && create.indexOf('Error') >= 0)
      {
        this.tools.showAlert(`Error`, create.code.replace('auth/',''));
        this.disableSubmit=false;
        this.tools.hideLoading();
        return false;
      }
      else
      { 
        const countryInfo = await this.tools.getFromLocal('countryInfo');
        const newUsr = new UserModel();
        newUsr.email = _email;
        newUsr.name = `Guest_${Date.now()}`;
        newUsr.id = create.uid;
        if (countryInfo)
        {
          newUsr.country=countryInfo.countryname;
          newUsr.countrycode=countryInfo.countrycode;
          newUsr.currencyCode=countryInfo.currencycode;
          newUsr.currencySymbol=countryInfo.currencysymbol;
        }
        
        const _state=await this.tools.getFromLocal('state');
        if (_state) newUsr.state = _state;

        this.userSrv.add(newUsr)
          .then(async (u)=>{
            console.log(u);
            this.userSrv.updateUserObj(newUsr);
            this.tools.hideLoading();
            this.router.navigateByUrl('/tabs/home')})
          .catch((e)=>{
            this.tools.showAlert(`Error`, e);
            console.warn(e);
            this.tools.hideLoading();
            this.disableSubmit=false;
            return false;
          })
      }
    }
    catch (error) { 
      console.log(error); 
      this.tools.showAlert('Error','There was an error creating an account')
      this.tools.hideLoading();}
  }

  async guest()
  {
    try {
      await this.tools.showLoading();
      await this.auth.loginAsGuest();
      this.tools.hideLoading();
      this.router.navigateByUrl('/tabs/home'); }
    catch (error) { 
      console.log(error); 
      this.tools.hideLoading();}
  }

  async resetPassword(email: string) {
    try {
      await this.tools.showLoading();
      await this.auth.forgotpassword(email);
      console.log('Email Sent'); 
      this.tools.hideLoading();
      this.router.navigateByUrl('login');
      }
    catch (error) { 
      console.log('Error: ', error); 
      this.tools.hideLoading();
    }
  }

  handleUserCredentials(userCredentials) {
    // This method gets the form value from the authentication component
    // And depending on the URL, it calls the respective method.
    const { email, password } = userCredentials;
    switch (this.url)
    {
      case 'login':
        this.login(email, password);
        break;
      case 'signup':
        this.signUp(email, password);
        break;
      case 'reset':
        this.resetPassword(email);
        break;
      case 'guest':
        this.resetPassword(email);
        break;
      default:
        this.login(email, password);
        break;
    } }
}
