import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusinessSendQuotePageRoutingModule } from './business-send-quote-routing.module';

import { BusinessSendQuotePage } from './business-send-quote.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusinessSendQuotePageRoutingModule
  ],
  declarations: [BusinessSendQuotePage]
})
export class BusinessSendQuotePageModule {}
