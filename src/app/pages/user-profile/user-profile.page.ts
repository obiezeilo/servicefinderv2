import { Component, OnInit } from '@angular/core';
import {ToolboxService } from '@services/toolbox.service';
import { Validators, FormBuilder, FormGroup} from '@angular/forms';
import { UserService } from '@services/user.service';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage implements OnInit {
  public userForm: FormGroup;

  countries: Country[] = [];
  locationChanged= false;
  validLocation= false;
  countryinfo:any;
  states: any;

  constructor(public formBuilder: FormBuilder, private tools: ToolboxService
    ,         private userSrv: UserService )
  {
    const u = this.userSrv.getUser();    
    console.log(u);

    this.userForm = this.formBuilder.group({
      userId: [u.id],
      name: [u.name, Validators.compose([Validators.required])],
      city: [u.city],
      state: [u.state],
      zip: [u.zip],
      country: [u.country, Validators.compose([Validators.required])],
      countrycode: [u.countrycode, Validators.compose([Validators.required])],
      currencyCode:[u.currencyCode],
      currencySymbol:[u.currencySymbol],
      email: [u.email, Validators.compose([Validators.required])],
      deviceToken: [u.deviceToken, Validators.compose([Validators.required])],
      latitude: [u.latitude],
      longitude: [u.longitude]
    });
    console.log(this.validLocation);
    this.validLocation =  ((this.userForm.value.city !== '' && this.userForm.value.state !== '') || this.userForm.value.zip !== '');

    this.userForm.valueChanges.subscribe((u) => {
      this.validLocation =  ((this.userForm.value.city !== '' && this.userForm.value.state !== '') || this.userForm.value.zip !== '');
      // console.log(u);
    });

    this.userForm.controls.city.valueChanges.subscribe(() => {
      this.locationChanged = true;
      // console.log("city changed");
    });
    this.userForm.controls.state.valueChanges.subscribe(() => {
      this.locationChanged = true;
      // console.log("state changed");
    });
    this.userForm.controls.zip.valueChanges.subscribe(() => {
      this.locationChanged = true;
      // console.log("zip changed");
    });
    this.userForm.controls.countrycode.valueChanges.subscribe(() => {
      this.locationChanged = true;
      this.userForm.controls.country.setValue(this.countries.find(o => o.countrycode === this.userForm.controls.countrycode.value).countryname);
    });
  }

  ngOnInit() { 
    console.log('ngoninit user', this.userSrv.getUser()) ;
    this.getLocationInfo();
  }

  async getLocationInfo()
  {
    this.countries = await this.tools.getCountries();
    this.countryinfo = await this.tools.getFromLocal('countryInfo');
    
    if (this.countryinfo == null)
    {
      this.countryinfo=this.countries.filter((c)=>c.countrycode == this.userForm.controls.countrycode.value.toUpperCase())[0];
    }    
    
    this.states = await this.tools.getStates(this.userForm.controls.countrycode.value);
    console.log(this.countryinfo, this.countries);
  }

  async setCountry(country:any)
  { 
    this.states = await this.tools.getStates(country);
  }

  async save() {
    // validated
    if (!this.validLocation) {
      this.tools.showAlert('Location required', 'Please provide your location');
      this.validLocation = false;
      return false;
    }

    const addr = environment.geocoding.url + 'address=' +this.userForm.value.city + ', ' + this.userForm.value.state + ' ' + this.userForm.value.zip
      + ', ' + this.userForm.value.country + '&key=' + environment.geocoding.api_key;
    // console.log(addr);

    if (this.locationChanged) { 
      const latlng = await this.tools.getLatLng(addr);
      console.log(latlng);
      if (!latlng)
      {
        this.tools.showAlert('Incorrect location','Your location could not be determined. Please confirm your city, state and/or zip/postal code');
        this.validLocation = false;
        return false;
      }
      this.userForm.value.latitude = latlng[0];
      this.userForm.value.longitude = latlng[1];
      await this.tools.saveToLocal('countryInfo', this.countryinfo);
    }
    this.userForm.controls.currencyCode.setValue(this.countryinfo.currencycode);
    this.userForm.controls.currencySymbol.setValue(this.countryinfo.currencysymbol);
    
    // console.log(this.userForm.value);return false;
    this.userSrv.update(this.userForm.value.userId, this.userForm.value)
      .then(async () =>{
        const u = await this.userSrv.getById(this.userForm.value.userId);
        this.userSrv.updateUserObj(u);
      })
      .then(() => {
        this.tools.showToast('User updated');
        this.tools.goto('/tabs/settings');
      }).catch((e) => {
        console.warn('Error: ', e)
        this.tools.showAlert('Error saving','There was an error saving changes');
      });
  }
}

interface Country {
  countrycode: string
  countryname: string
  currencycode: string
  currencyname: string
  currencysymbol: string
}
