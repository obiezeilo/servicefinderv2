import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-auth-form',
  templateUrl: './auth-form.component.html',
  styleUrls: ['./auth-form.component.scss'],
})
export class AuthFormComponent implements OnInit {
  @Input() actionButtonText = 'Sign In';
  @Input() isPasswordResetPage = false;
  @Input() disableSubmit=false;
  @Output() formSubmitted = new EventEmitter<any>();
  public authForm: FormGroup;

  constructor(private readonly formBuilder: FormBuilder) { }

  ngOnInit() {
    this.initializeForm(!this.isPasswordResetPage);
  }

  initializeForm(showPasswordField: boolean): void {
    this.authForm = this.formBuilder.group({
        email: ['', Validators.compose([Validators.required,
          Validators.email])],
        password: ['', Validators.compose([showPasswordField ?
          Validators.required : null, Validators.minLength(6)])], 
      });
    this.authForm.valueChanges.subscribe((s)=>{
      this.disableSubmit = !this.authForm.valid;
    })
  }

  submitCredentials(authForm: FormGroup): void { 
    if (!authForm.valid) {
      console.log('Form is not valid yet, current value:', authForm.value); }
    else{
      this.disableSubmit=true;
      const credentials = {
        email: authForm.value.email,
        password: authForm.value.password};
      this.formSubmitted.emit(credentials); }
    }
  
    public getErrorMessage(form: FormGroup, controlName: string): string
    {
      if (controlName === 'fullName') {
      return (form.get(controlName).hasError('required') ? 'Please Enter Fullname' : ' ' || form.get(controlName).hasError('maxLength') ? 'Please required length' : '');
      }
      if(controlName === 'email') {
      return (form.get(controlName).hasError('required') ? 'Please Enter Email' : ' ' || form.get(controlName).hasError('maxLength') ? 'Please required length' : '');
      }
      if (controlName === 'phoneNumber') {
      return (form.get(controlName).hasError('required') ? 'Please Enter phone number' : ' '
      || form.get(controlName).hasError('maxLength') ? 'Please EnterName':'');
      }
      if (controlName === 'desciption') {
      return form.get(controlName).hasError('maxLength') ? 'Please required length' : '';
      }
      if (controlName === 'password') {
      return (form.get(controlName).hasError('required') ? 'Please Enter    Password' : ' ' || form.get(controlName).hasError('maxLength') ? 'Please required length' : '');
      }
    }
}
