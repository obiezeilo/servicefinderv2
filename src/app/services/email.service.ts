import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  constructor() { }

  async sendEmail(_from = 'info@servicefinder.com', _fromName = 'Service Finder support ', 
    _to, _toName='', _subject, _textBody, _htmlBody = '', _template='', _templatevars='')
  {
    try {
        const emailApi = 'https://obiezeilo.com/zmail/index.php'; // 'https://mg.zigozi.co/sendmj.php'
        const response = await fetch(emailApi, {
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            body: JSON.stringify({
                from: _from,
                fromName: _fromName,
                to: _to,
                toName: _toName,
                subject: _subject,
                text: _textBody,
                html: _htmlBody,
                template: _template,
                templatevars: _templatevars
            })
        });
        // response.json().then((r)=> console.log('res', r)).catch((er)=>{console.warn(er);});
        return await response.json();
    } catch (error) {
        console.warn('sendmail error', error);
        return error;
    }
  }
}
