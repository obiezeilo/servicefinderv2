import { Component, OnInit, OnDestroy } from '@angular/core';
import {RequestService} from '@services/request.service';
import Request from '@models/request';
import { BehaviorSubject } from 'rxjs';
import {ToolboxService } from '@services/toolbox.service';
import { UserModel } from '@models/user';
import { ModalController } from '@ionic/angular';
import { RequestFormPage } from '@pages/request-form/request-form.page';
import { StorageService } from '@services/storage.service';
import { AuthenticationService } from '@auth/authentication.service';
import { UserService } from '@services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  userObj: UserModel;
  theTestVar:string;
  requests$: BehaviorSubject<Request[]>;

  constructor( private tools: ToolboxService, public userSrv: UserService,
               private reqService: RequestService, private auth: AuthenticationService,
               public modalController: ModalController, private storageService: StorageService
  ) {this.storageService.get('deviceToken').then((t)=>{console.log('tokenhome:', t);})}

  ngOnInit() {
    if (!this.userSrv.getUser()) this.userSrv.refreshUserSubject();  // to load the user into the subject for subsequent pages
    this.getRequests();
  }

  async getRequests()
  {
    const usr = this.auth.getUser;
    console.log('inside getloggedinuser: ', usr)
    const arr = [];
    arr.push({property:'status', operator:'!=', value:'completed'});
    arr.push({property:'userId', operator:'==', value:usr.uid});

    this.requests$ = this.reqService.getSub$(arr, null, ['status']);
    this.requests$.subscribe((reqs) => {
      if (reqs)
      reqs.map((req)=>{
        req['timeago'] = (req.status === 'new') ? this.tools.timeAgo(req.createdDate) : this.tools.timeAgo(req.statusDate);
      })
    });
  }

  logout() {
    this.tools.logOut();
  }

  async gotoRequestForm() {
    try {
      const modal = await this.modalController.create({
        component: RequestFormPage,
        componentProps: { isNew: true}
      });
      modal.onDidDismiss().then((obj) => {
        if (obj.data.dismissed === false) {
          console.log('new request created');
        } else {
          console.log('cancelled Editing request');
        }
      });
      return await modal.present();

    } catch (error) {
      this.tools.showAlert('Error', error);
    }
  }


  ionViewDidEnter() {
    console.log('home ionViewDidEnter');
    console.log('authuser:', this.auth.getUser);
    // this.getRequests();
  }

  ngOnDestroy() {
    console.log('home ondestroy');
    if (this.userObj) this.userObj = null;
    this.requests$.next(null);
    this.requests$.complete();
  }
}
