import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CompletedRequestsPageRoutingModule } from './completed-requests-routing.module';
import { ComponentsModule } from '../../components/components.module';

import { CompletedRequestsPage } from './completed-requests.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,ComponentsModule,
    CompletedRequestsPageRoutingModule
  ],
  declarations: [CompletedRequestsPage]
})
export class CompletedRequestsPageModule {}
