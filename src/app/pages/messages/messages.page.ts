import { Component, OnInit } from '@angular/core';
import { Message, Chat } from '@models/message';
import { MessageService } from '@services/message.service';
import {ToolboxService} from '@services/toolbox.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '@auth/authentication.service';
import { RequestService } from '@services/request.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.page.html',
  styleUrls: ['./messages.page.scss'],
})
export class MessagesPage implements OnInit {
  messageObj:Message = new Message();
  newChat:Chat = new Chat();
  receiver='';
  requestId='';
  chats:Chat[] = [];
  userId = '';
  bizLogo='https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y';
  frombiz = '';
  isCompleted=false;

  constructor(private msgSrv:MessageService, private route: ActivatedRoute
    , private router: Router, private auth: AuthenticationService
    , private tools: ToolboxService, private requestService: RequestService)
  {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state)
      {
        console.log(this.router.getCurrentNavigation().extras.state.params);
        this.requestId=this.router.getCurrentNavigation().extras.state.params.requestId;
        this.frombiz=this.router.getCurrentNavigation().extras.state.params.fromBiz
        this.refreshMessages();
      }
      else
      this.tools.goto('/messages-list');
    });

  }

  async refreshMessages()
  {
    this.msgSrv.getById$(this.requestId)
      .subscribe((m) => {
        this.messageObj = m as Message;
        this.receiver= this.frombiz ? this.messageObj.userName : this.messageObj.businessName;
        if (this.messageObj.businessLogo) this.bizLogo=this.messageObj.businessLogo;
        this.chats=this.messageObj.chats as Chat[];
      });
  }

  ngOnInit()
  {
    this.userId = this.auth.getUser.uid;
    console.log(this.userId);
    this.newChat.read=false;
    this.requestService.getById(this.requestId).then((r)=>{
      this.isCompleted = (r.status=='completed') ;
      console.log(this.isCompleted);
    })
  }

  async send()
  {
    this.newChat.createdDate=Date.now();
    this.newChat.sender = (this.messageObj.userId === this.userId) ? 'user' : 'business';

    await this.msgSrv.addChat(this.requestId, this.newChat)
    this.newChat=new Chat();
  }
  
  showHowLongAgo(tstamp)
  {
    return this.tools.timeAgo(tstamp);
  }
}
