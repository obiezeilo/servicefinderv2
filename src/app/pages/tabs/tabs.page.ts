import { Component, OnInit } from '@angular/core';
import { MessageService } from '@services/message.service';
import { AuthenticationService } from '@auth/authentication.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {
  howManyUnreads = 0;
  messagesSubscription: Subscription;
  constructor(private msgSrv: MessageService, private authService: AuthenticationService) { }

  async ngOnInit() {
    const u = await this.authService.getUser;
    if (u == undefined) { return; }
    this.messagesSubscription = this.msgSrv.get$([{property:'userId', operator:'==', value:u.uid}])
      .subscribe((m:any) => {
        this.howManyUnreads = 0;
        for (let i = 0; i < m.length; i++) {
          for (let j = 0; j < m[i].chats.length; j++) {
            if ((m[i].chats[j].sender === 'user' && m[i].userId !== u.uid && m[i].chats[j].read === false)
              || (m[i].chats[j].sender === 'business' && m[i].businessId !== u.uid && m[i].chats[j].read === false))
            { this.howManyUnreads++; }
          }
        }
    });
  }

  ngOnDestroy() {
    this.messagesSubscription.unsubscribe();
  }
}