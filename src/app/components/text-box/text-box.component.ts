import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-text-box',
  templateUrl: './text-box.component.html',
  styleUrls: ['./text-box.component.scss'],
})
export class TextBoxComponent implements OnInit {
  @Input() parentForm: FormGroup;
  @Input() controlName: string;
  @Input() placeholder: '';
  @Input() type: 'text';
  @Input() label: '';
  @Input() maxLength: number;
  @Input() isTextArea = false;
  @Input() getErrorMessage: (form: FormGroup, controlName: string) => string;
  
  constructor() { }

  ngOnInit() {}

}
