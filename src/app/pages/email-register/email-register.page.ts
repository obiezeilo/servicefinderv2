import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthenticateService } from '@services/auth.service';
import { NavController } from '@ionic/angular';
import { UserService } from '@services/user.service';
import { UserModel } from '@models/user';
import {ToolboxService } from '@services/toolbox.service';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-email-register',
  templateUrl: './email-register.page.html',
  styleUrls: ['./email-register.page.scss'],
})
export class EmailRegisterPage implements OnInit {


  validations_form: FormGroup;
  errorMessage: string = '';
  successMessage: string = '';

  validation_messages = {
   'email': [
     { type: 'required', message: 'Email is required.' },
     { type: 'pattern', message: 'Enter a valid email.' }
   ],
   'password': [
     { type: 'required', message: 'Password is required.' },
     { type: 'minlength', message: 'Password must be at least 5 characters long.' },
   ],
   'confirmpassword': [
    { type: 'required', message: 'Confirm Password is required.' },
    { type: 'minlength', message: 'Password must be at least 5 characters long.' }
  ],
   'name': [
    { type: 'required', message: 'Your name is required.' },
    { type: 'minlength', message: 'Name must be at least 2 characters long.' }
  ],
  /* 'state': [
   { type: 'maxLength', message: 'Enter a 2-letter abbreviation of your state' },
   { type: 'pattern', message: 'Must be capitalized and 2 letters' }
 ] */
 };

  constructor(
    private navCtrl: NavController,
    private authService: AuthenticateService,
    private formBuilder: FormBuilder,
    private userService: UserService, private tools:ToolboxService
  ) {}

  ngOnInit(){
    this.validations_form = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
      confirmpassword: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
      name: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(2)
      ]))/*,
      city: new FormControl('', Validators.minLength(2)),
      state: new FormControl('', Validators.compose([
        Validators.maxLength(2),
        Validators.pattern('^[A-Z]+$')
      ])),
      zip: new FormControl('', Validators.compose([
        Validators.pattern('^[a-zA-Z0-9_.+-]+$')
      ])),
      country: new FormControl('', Validators.required)*/
    },{ 
      validators: this.password.bind(this)
    });
  }

  tryRegister(value){
    console.log(value);
    // return false;
    const usr=new UserModel;

    this.authService.registerUser(value)
     .then(res => {
       console.log(res);
       if (res.message)
       {
         this.tools.showAlert("Error", res.message);
          throw res.message;
       }
       else return (res);
       
     }, err => {
       console.log(err);
       this.errorMessage = err.message;
       this.successMessage = "";
     }).then(async res=>{
       console.log("res is ", res);
       usr.name=value.name;
       usr.city=value.city;
       usr.state=value.state;
       usr.zip=value.zip;
       usr.country=value.country;
       usr.email=value.email;
       usr.id=res.uid;
       // get the lat/lng
       let addr=environment.geocoding.url + 'address=' +usr.city + ', ' + usr.state + ' ' + usr.zip
          + ', ' + usr.country + '&key=' + environment.geocoding.api_key;
        var latlng=await this.tools.getLatLng(addr);
        usr.latitude=latlng[0];
        usr.longitude=latlng[1];
       return this.userService.add(usr);
     })
     .then((u)=>{
      console.log(u);
      // this.tools.showToast("Account created");
      // this.errorMessage = "";
      // this.successMessage = "Your account has been created. Please log in.";
      this.tools.showAlert("Account created","Your account has been created. Please log in.")
        .then(()=>{
          this.tools.goto("start",null,true);
        })
        .catch((e)=>{
          this.tools.showAlert("Error", JSON.stringify(e));
        })
     })
     .catch((err)=>{
      console.log(err);
      this.errorMessage = err.message;
      this.successMessage = "";
      this.tools.showAlert("Error", err);
     })
     /**/
  }

  password(formGroup: FormGroup) {
    const { value: password } = formGroup.get('password');
    const { value: confirmPassword } = formGroup.get('confirmpassword');
    return password === confirmPassword ? null : { passwordNotMatch: true };
  }

  goLoginPage(){
    this.navCtrl.back();
  }
}
