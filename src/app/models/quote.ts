export interface Quote {
    id?:string;
    requestId:string;
}

export class Quote {
    id?: string;
    requestId: string;
    businessName:string;
    businessId: string;
    businessLocation: string;
    businessRating: number;
    businessRatingCount: number;
    businessLogo:string;
    businessPhone:string;
    businessEmail:string;
    amount: number;
    note: string;
    createdDate: number;
    status:string;
    statusReason:string;
    statusDate:number;
    rating:number;
    ratingNotes:string;
}