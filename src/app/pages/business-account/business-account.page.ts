import { Component, OnInit, OnDestroy } from '@angular/core';
import {RequestService} from '@services/request.service';
import {QuoteService} from '@services/quote.service';
import {Business} from '@models/business';
import Request from '@models/request';
import {ToolboxService} from '@services/toolbox.service';
import { Quote } from '@models/quote';
import { Message } from '@models/message';
import { MessageService } from '@services/message.service';
import { BehaviorSubject } from 'rxjs';
import { BusinessService } from '@services/business.service';
import { AuthenticationService } from '@auth/authentication.service';

@Component({
  selector: 'app-business-account',
  templateUrl: './business-account.page.html',
  styleUrls: ['./business-account.page.scss'],
})
export class BusinessAccountPage implements OnInit, OnDestroy {

  business: Business;
  categories: string;
  requests: Request[];
  quotes: Quote[];
  segment: string;
  messages: Message[];
  bizFromLocal: any;
  isGuest = false;
  requests$: BehaviorSubject<Request[]>;
  newRequests$: BehaviorSubject<Request[]>;
  completedRequests$: BehaviorSubject<Request[]>;
  public currency:string;

  constructor(private msgSrv: MessageService, private auth: AuthenticationService
    , public businessSrv: BusinessService, public tools: ToolboxService
    , private requestSrv: RequestService, private quoteSrv: QuoteService) {}

  ngOnInit() {
    if (!this.auth.getUser) this.tools.goto('/start', true);
    
   this.reloadData();
  }

  ionViewDidEnter() {
   console.log('ionViewDidEnter');
   this.reloadData();
  }

  reloadData(event = null)
  {
    this.segment = 'requests';
    this.getInfo();
    if (event && event.target) event.target.complete();
  }
  
  async getInfo() {
    this.currency=await this.tools.getCurrency();
    if (this.businessSrv.businessSubject$.value == null) return;

    this.categories = this.businessSrv.businessSubject$.value.category.join(', ');
    this.msgSrv.get$([{property:'businessId', operator:'==', value:this.businessSrv.businessSubject$.value.id}])
      .subscribe((m)=>{
        this.messages = m;
      });
    // get the quotes
    let arr = [], newReqs = [], completedReqs = [];
    arr.push('businessId|==|' + this.businessSrv.businessSubject$.value.id);
    
    // Get the requests
    newReqs.push({property:'category', operator:'in', value:this.businessSrv.businessSubject$.value.category});
    newReqs.push({property:'status', operator:'==', value:'new'});
    newReqs.push({property:'state', operator:'==', value:this.businessSrv.businessSubject$.value.state});
    // Completed requests
    completedReqs.push({property:'category', operator:'in', value:this.businessSrv.businessSubject$.value.category});
    completedReqs.push({property:'status', operator:'==', value:'completed'});
    completedReqs.push({property:'businessId', operator:'==', value:this.businessSrv.businessSubject$.value.id});
          
    this.newRequests$ = this.requestSrv.getSub$(newReqs);
    this.completedRequests$ = this.requestSrv.getSub$(completedReqs);

    this.quoteSrv.get(arr).then((q) => {
      this.quotes = q as Quote[];

      this.newRequests$.subscribe((reqs)=>{
        if (reqs)
        {  reqs.map((r)=>{
            for (q of this.quotes)
            {
              if (q.requestId === r.id) {
                r['quote'] = q.amount;
                r['quoteStatus'] = q.status;
                break;
              }
            }
          })
        }
      })
      this.completedRequests$.subscribe((r)=>{
      })
    });// the quotes
  }

  gotoDetails(req) {
    this.tools.goto('business-request-details', req);
  }

  goToAddEdit() {
    this.tools.goto('business-account-form');
  }

  loadCredit() {
    this.tools.goto('load-credits');
  }

  gotoMessage(m) {
    this.tools.goto('messages', {requestId: m.requestId, fromBiz: true});
  }

  trackByFn(index: number, request: Request)
  {
    return request.id;
  }

  ngOnDestroy() {
    try {
      this.business = null;
      if (this.newRequests$)
      {
        this.newRequests$.next(null);
        this.newRequests$.complete();
      }
      if (this.completedRequests$)
      {
        this.completedRequests$.next(null);
        this.completedRequests$.complete();
      }
    } catch (error) {
      console.warn(error);
    }
  }
}