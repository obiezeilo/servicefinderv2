import { Component, OnInit } from '@angular/core';
import {BusinessService} from '@services/business.service';
import { Business } from '@models/business';
import { StorageService } from '@services/storage.service';

@Component({
  selector: 'app-credit-history',
  templateUrl: './credit-history.page.html',
  styleUrls: ['./credit-history.page.scss'],
})
export class CreditHistoryPage implements OnInit {
  business:Business = new Business;
  history: any[];
  currency:string;

  constructor(private businessSrv: BusinessService, private storageService: StorageService)
  {
    console.log("constructor");
  }

  async setCurrency()
  {
    this.currency=(await this.storageService.get('countryInfo')).currencysymbol;
  }

  ngOnInit() {
    this.business = this.businessSrv.getBusiness();
    var a = this.business.creditLoadHistory as Array<any>;
    if (a) this.history = a.sort((a,b)=>b.createdDate - a.createdDate);
    this.setCurrency()
  }
}