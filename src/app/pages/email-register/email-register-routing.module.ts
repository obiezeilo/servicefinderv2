import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmailRegisterPage } from './email-register.page';

const routes: Routes = [
  {
    path: '',
    component: EmailRegisterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmailRegisterPageRoutingModule {}
